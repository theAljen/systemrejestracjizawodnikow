﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using SystemZarzadzaniaZawodami.Contract;

namespace SystemZarzadzaniaZawodami.Implementation
{
    public class EdycjaZawodow : IZawody
    {
        public void EdytujZawody(string nazwa, string termin, string opis, string limit, string dyscyplina, int idZawodow)
        {
            string Polece0Sql = String.Format(
                                @"UPDATE [dbo].[Zawody]
                                 SET [Nazwa]='{0}',[Termin]='{1}',[Opis]='{2}', [LimitZawodnikow]='{3}', [Dyscyplina]='{4}'                 
                                 WHERE ZawodyID = '{5}'", nazwa, termin, opis, limit, dyscyplina, idZawodow);

            //string path = System.Reflection.Assembly.GetExecutingAssembly().CodeBase; var directory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);
            SqlConnection sc = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodow.mdf" + @";Integrated Security=True");
            SqlCommand kwerenda;

            try
            {
                kwerenda = new SqlCommand(Polece0Sql);

                sc.Open();

                kwerenda.Connection = sc;
                kwerenda.ExecuteNonQuery();

                sc.Close();

                MessageBox.Show("Dane dotyczące wybranych zawodów zostały zmienione");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        
        public void EdytujZawodnika(int id, string imie, string nazwisko, string plec, string wiek, string waga, string badania, string licencja, string kraj, string klub, string mail, string zawody)
        {
            string PolecenieSql = String.Format(
                      @"UPDATE {11} SET
                                       [Imie]='{0}',[Nazwisko]='{1}',[Plec]='{2}',[KategoriaWiekowa]='{3}',[KategoriaWagowa]='{4}',[Badania]='{5}',[NumerLicencji]='{6}',[Kraj]='{7}',[Klub]='{8}',[Mail]='{9}' 
                                       WHERE ZawodnikID = '{10}'",
                               imie, nazwisko, plec, wiek, waga, badania, licencja, kraj, klub, mail, id, zawody);


            //string path = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            //var directory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);
            SqlConnection sc = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");
            SqlCommand kwerenda;

            try
            {
                kwerenda = new SqlCommand(PolecenieSql);

                sc.Open();

                kwerenda.Connection = sc;
                kwerenda.ExecuteNonQuery();

                sc.Close();

                MessageBox.Show("Edycja zawodnika przebiegła pomyślnie");

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
