﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SystemZarzadzaniaZawodami.Contract;

namespace SystemZarzadzaniaZawodami.Implementation
{
    public class LogikaZarzadzania : IAdmin
    {
        public void DodajZawody(string nazwa, string termin, string opis, string limit, string plec, string dyscyplina, string czy54, string czy57, string czy63, string czy69, string czy74, string czy79,
            string czy84, string czy89, string czy94, string czyjunior, string czykadet, string czysenior)
        {
            SqlCommand cmd;

            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection sc = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodow.mdf" + @";Integrated Security=True");
            sc.Open();
            string polecenie = String.Format(
                "INSERT INTO  Zawody ([ZawodyID],[Nazwa],[Termin],[Opis], [LimitZawodnikow], [Dyscyplina], [Czy54], [Czy57], [Czy63], [Czy69], [Czy74],[Czy79], [Czy84], [Czy89], [Czy94], [CzySenior], [CzyJunior], [CzyKadet], [Plec] ) VALUES (@ZawodyID, @Nazwa, @Termin, @Opis, @LimitZawodnikow, @Dyscyplina, @Czy54, @Czy57, @Czy63, @Czy69, @Czy74, @Czy79, @Czy84, @Czy89, @Czy94, @CzySenior, @CzyJunior, @CzyKadet, @Plec)");
            cmd = new SqlCommand(polecenie, sc);
            sc.Close();
            int[] tablica = new int[300];

            String queryString = "SELECT ZawodyID From [Zawody]";
            String query = String.Format(queryString);
            SqlCommand abc = new SqlCommand(query, sc);
            sc.Open();
            SqlDataReader myreader = abc.ExecuteReader();
            while (myreader.Read())
            {
                string id = myreader["ZawodyID"].ToString();
                tablica[Convert.ToInt32(id) - 1] = Convert.ToInt32(id); 

            }
            sc.Close();

            sc.Open();

            int zawodyID = tablica.Max() + 1;
            try
            {
                cmd.Parameters.AddWithValue("@ZawodyID", zawodyID);
                cmd.Parameters.AddWithValue("@Nazwa", nazwa);
                cmd.Parameters.AddWithValue("@Termin", termin);
                cmd.Parameters.AddWithValue("@Opis", opis);
                cmd.Parameters.AddWithValue("@LimitZawodnikow", limit);
                cmd.Parameters.AddWithValue("@Dyscyplina", dyscyplina);
                cmd.Parameters.AddWithValue("@Czy54", czy54);
                cmd.Parameters.AddWithValue("@Czy57", czy57);
                cmd.Parameters.AddWithValue("@Czy63", czy63);
                cmd.Parameters.AddWithValue("@Czy69", czy69);
                cmd.Parameters.AddWithValue("@Czy74", czy74);
                cmd.Parameters.AddWithValue("@Czy79", czy79);
                cmd.Parameters.AddWithValue("@Czy84", czy84);
                cmd.Parameters.AddWithValue("@Czy89", czy89);
                cmd.Parameters.AddWithValue("@Czy94", czy94);
                cmd.Parameters.AddWithValue("@CzySenior", czysenior);
                cmd.Parameters.AddWithValue("@CzyJunior", czyjunior);
                cmd.Parameters.AddWithValue("@CzyKadet", czykadet);
                cmd.Parameters.AddWithValue("@Plec", plec);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

            //Tworzenie tabel w BazaZawodnnikow:

            SqlCommand cmd3;
            string directory3 = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory3 = directory.Substring(0, directory3.Length - 14);

            SqlConnection sc3 = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory3 + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");
            SqlDataReader reader3;

            string nazwaTabeliZawodnikow = "Zawody" + Convert.ToString(zawodyID);

            sc3.Open();
            string polecenie3 = String.Format("CREATE TABLE {0} (ZawodnikID int NOT NULL, Imie varchar(30) NOT NULL, Nazwisko varchar(30) NOT NULL, Plec varchar(1), KategoriaWagowa int, KategoriaWiekowa varchar(30), Badania bit, NumerLicencji varchar(30), Kraj varchar(30), Klub varchar(30), Mail varchar(30))", nazwaTabeliZawodnikow);

            cmd3 = new SqlCommand(polecenie3, sc3);
            reader3 = cmd3.ExecuteReader();
            sc3.Close();


            //Tworzenie tabel w BazaLosowan:

            string waga = "";
            string wiek = "";

            SqlCommand cmd2;
            string directory2 = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory2 = directory.Substring(0, directory2.Length - 14);

            SqlConnection sc2 = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory2 + @"\BazyDanych\BazaLosowan.mdf" + @";Integrated Security=True");
            string nazwaTabeli = "Zawody" + Convert.ToString(zawodyID) + plec + "Kat" + waga + wiek;

            SqlDataReader reader;

            List<string> listaWag = new List<string>();
            List<string> listaWiekow = new List<string>();
            List<string> listaPlci = new List<string>();
            try
            {
                if (plec == "1")
                {
                    listaPlci.Add("M");
                }
                else if (plec == "0")
                {
                    listaPlci.Add("K");
                }

                if (czyjunior == "1")
                {
                    listaWiekow.Add("JNR");
                }
                if (czysenior == "1")
                {
                    listaWiekow.Add("SNR");
                }
                if (czykadet == "1")
                {
                    listaWiekow.Add("KDT");
                }

                if (czy54 == "1")
                {
                    listaWag.Add("54");
                }
                if (czy57 == "1")
                {
                    listaWag.Add("57");
                }
                if (czy63 == "1")
                {
                    listaWag.Add("63");
                }
                if (czy69 == "1")
                {
                    listaWag.Add("69");
                }
                if (czy74 == "1")
                {
                    listaWag.Add("74");
                }
                if (czy79 == "1")
                {
                    listaWag.Add("79");
                }
                if (czy84 == "1")
                {
                    listaWag.Add("84");
                }
                if (czy89 == "1")
                {
                    listaWag.Add("89");
                }
                if (czy94 == "1")
                {
                    listaWag.Add("94");
                }

                foreach (string wag in listaWag)
                {
                    waga = wag;

                    foreach (string wie in listaWiekow)
                    {
                        wiek = wie;

                        foreach (string pl in listaPlci)
                        {
                            plec = pl;
                            nazwaTabeli = "Zawody" + Convert.ToString(zawodyID) + plec + "Kat" + waga + wiek;

                            sc2.Open();
                            string polecenie2 = String.Format("CREATE TABLE {0} (Pozycja int, ImieNazwisko varchar(50))", nazwaTabeli);
                            cmd2 = new SqlCommand(polecenie2, sc2);
                            reader = cmd2.ExecuteReader();
                            sc2.Close();

                        }//end foreach
                    }//endforeach
                }///end foreach
                MessageBox.Show("Zawody zostały dodane pomyślnie!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
