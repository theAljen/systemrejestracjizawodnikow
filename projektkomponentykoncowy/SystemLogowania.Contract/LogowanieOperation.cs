﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace SystemLogowania.Contract
{
    [DataContract]
    public class LogowanieOperation
    {
        [DataMember]
        public bool Zalogowano { get; private set; }

        [DataMember]
        public string Login { get; private set; }

        [DataMember]
        public string Haslo { get; private set; }

        [DataMember]
        public string RodzajUprawnien { get; private set; }

        public LogowanieOperation(bool Zalogowano, string Login, string Haslo, string RodzajUprawnien)
        {
            this.Zalogowano = Zalogowano;
            this.Login = Login;
            this.Haslo = Haslo;
            this.RodzajUprawnien = RodzajUprawnien;
        }
    }
}
