﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace SystemLogowania.Contract
{
    [ServiceContract]
    public interface IUzytkownicy
    {
        [OperationContract]
        string PobierzAutorow();

        [OperationContract]
        LogowanieOperation Zaloguj(string Login, string Haslo);

        [OperationContract]
        void Zarejestruj(string login, string haslo, string uprawnienia);
    }
}
