﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Ninject.Extensions.Conventions;
using Ninject.Extensions.Wcf;
using SystemKalendarza.Contract;
using SystemKalendarza.Implementation;

namespace SystemKalendarza.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel();
            kernel.Bind<IKalendarz>().To<Kalendarz>();

            kernel.Bind(x => x
                .FromAssemblyContaining<Kalendarz>()
                .IncludingNonePublicTypes()
                .SelectAllClasses()
                .InheritedFrom<IKalendarz>()
                .BindAllInterfaces()
                .Configure(b => b.InSingletonScope()));

            using (var host = kernel.Get<NinjectServiceHost<Kalendarz>>())
            {
                host.Open();

                Console.WriteLine("HOST: Kalendarz Imprez");
                Console.WriteLine("the service is ready at {0}", host.BaseAddresses[0]);
                Console.WriteLine("press <enter> to stop the service.");
                Console.ReadLine();

                host.Close();
            }
        }
    }
}
