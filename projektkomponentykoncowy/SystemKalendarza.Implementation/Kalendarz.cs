﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemKalendarza.Contract;

namespace SystemKalendarza.Implementation
{
    public class Kalendarz : IKalendarz
    {
        public List<Zawody> pobierzListeZawodow()
        {
            List<Zawody> zawodyList = new List<Zawody>();

            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 32);
           
            
            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodow.mdf" + @";Integrated Security=True");

            String queryString = "SELECT * From Zawody ";
            String query = String.Format(queryString);
            SqlCommand abc = new SqlCommand(query, Polaczenie);
            Polaczenie.Open();
            SqlDataReader myreader = abc.ExecuteReader();
            Zawody zawody;
            while (myreader.Read())
            {
                zawody = new Zawody(myreader["Nazwa"].ToString(), myreader["Termin"].ToString(),
                    myreader["Opis"].ToString(), myreader["Dyscyplina"].ToString(),
                    myreader["LimitZawodnikow"].ToString(), myreader["ZawodyID"].ToString());
                zawodyList.Add(zawody);
            }

            return zawodyList;
        }
    }
}
