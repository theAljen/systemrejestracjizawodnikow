﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemZarzadzaniaZawodami.Contract
{
    public interface IZawody
    {
        void EdytujZawody(string nazwa, string termin, string opis, string limit, string dyscyplina, int idZawodow);
        void EdytujZawodnika(int id, string imie, string nazwisko, string plec, string wiek, string waga, string badania, string licencja, string kraj, string klub, string mail, string zawody);
    }
}
