﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemZarzadzaniaZawodami.Contract
{
    public interface IAdmin
    {
        void DodajZawody(string nazwa, string termin, string opis, string limit, string plec, string dyscyplina, string czy54, string czy57, string czy63, string czy69, string czy74, string czy79,
            string czy84, string czy89, string czy94, string czyjunior, string czykadet, string czysenior);
    }
}
