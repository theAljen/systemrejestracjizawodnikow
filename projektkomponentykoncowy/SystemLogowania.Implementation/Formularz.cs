﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using SystemLogowania.Contract;

namespace SystemLogowania.Implementation
{
    public class Formularz : IUzytkownicy
    {
        public string PobierzAutorow()
        {
            string TextOAutorze = "Michał Łabiński, Mateusz Mróz, Jarosław Eliasz";
            return TextOAutorze;
        }
       
        public void Zarejestruj(string login, string haslo, string uprawnienia)
        {
            //string path = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            //var directory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 31);
            SqlConnection ConDataBase = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaUzytkownikow.mdf" + @";Integrated Security=True");
            string query = "insert into dbo.Uzytkownicy (Login,Haslo,RodzajUprawnien) values ('" + login + "','" + haslo + "','" + uprawnienia + "');";
            SqlCommand cmdDataBase = new SqlCommand(query, ConDataBase);
            ConDataBase.Open();
            cmdDataBase.ExecuteNonQuery();
            ConDataBase.Close();
        }


        public LogowanieOperation Zaloguj(string Login, string Haslo)
        {
            LogowanieOperation logowanieOperation = null;


            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 31);
            
            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaUzytkownikow.mdf" + @";Integrated Security=True");
            String queryString = "SELECT * From [Uzytkownicy] WHERE Login ='{0}' and Haslo ='{1}'";
            String query = String.Format(queryString, Login, Haslo);
            SqlCommand abc = new SqlCommand(query, Polaczenie);

            Polaczenie.Open();
            SqlDataReader myreader = abc.ExecuteReader();

            if (myreader.Read())
            {
                string RodzajUprawnien = myreader["RodzajUprawnien"].ToString();
                logowanieOperation = new LogowanieOperation(true, Login, Haslo, RodzajUprawnien);
            }
            else
            {
                logowanieOperation = new LogowanieOperation(false, null, null, null);
            }

            return logowanieOperation;
        }
    }
}
