﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uzytkownicy.Contract;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace Uzytkownicy.Implementation
{
    public class Trener : ITrener
    {
        private int LiczbaRekordowWBazieZawodnikow(int idZawodow)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");
            int indeks = 0;

            string kwerenda = string.Format("SELECT * FROM {0}", "Zawody" + idZawodow);
            SqlCommand cmd = new SqlCommand(kwerenda, connection);

            connection.Open();
            SqlDataReader myreader = cmd.ExecuteReader();

            while (myreader.Read())
            {
                try
                {
                    indeks++;
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
            connection.Close();
            return indeks;
        }

        public void DodajZawodnika(int dodawanie, string imie, string nazwisko, string plec, string wiek, string waga, string badania, string licencja, string kraj, string klub, string mail)
        {
            SqlCommand cmd;

            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection sc = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");
            try
            {
                sc.Open();
                string polecenie = String.Format("INSERT INTO  {0} ([ZawodnikID],[Imie], [Nazwisko], [Plec], [KategoriaWiekowa], [KategoriaWagowa], [Badania], [NumerLicencji], [Kraj], [Klub], [Mail]) VALUES (@ZawodnikID, @Imie, @Nazwisko, @Plec,@KategoriaWiekowa, @KategoriaWagowa, @Badania, @NumerLicencji, @Kraj, @Klub, @Mail)", "Zawody" + Convert.ToInt32(dodawanie));
                cmd = new SqlCommand(polecenie, sc);

                cmd.Parameters.AddWithValue("@ZawodnikID", LiczbaRekordowWBazieZawodnikow(dodawanie) + 1);
                cmd.Parameters.AddWithValue("@Imie", imie);
                cmd.Parameters.AddWithValue("@Nazwisko", nazwisko);
                cmd.Parameters.AddWithValue("@Plec", plec);
                cmd.Parameters.AddWithValue("@KategoriaWiekowa", wiek);
                cmd.Parameters.AddWithValue("@KategoriaWagowa", waga);
                cmd.Parameters.AddWithValue("@Badania", badania);
                cmd.Parameters.AddWithValue("@NumerLicencji", licencja);
                cmd.Parameters.AddWithValue("@Kraj", kraj);
                cmd.Parameters.AddWithValue("@Klub", klub);
                cmd.Parameters.AddWithValue("@Mail", mail);
                cmd.ExecuteNonQuery();
                sc.Close();
                MessageBox.Show("Zawodnik zarejestrowany pomyślnie");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
