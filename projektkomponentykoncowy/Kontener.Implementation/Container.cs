﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;
using System.Reflection;

namespace Kontener.Implementation
{
    public class Container : IContainer
    {

        IDictionary<Type, Type> SlownikTypow;

        /// <summary>
        /// Registering class as provider of new instance of all implementing interfaces.
        /// Resolve method will return new instance of this class for each implemented interface.
        /// </summary>
        /// <param name="type">type of interface implementation to register</param>
        public void Register(Type type)
        {
            List<Type> Interfejsy = new List<Type>();

            foreach (var interfejs in type.GetInterfaces())
            {
                Interfejsy.Add(interfejs);
            }

            for (int i = 0; i < Interfejsy.Count; i++)
            {
                bool CzyZawieraKlucz = false;

                foreach (var klucz in SlownikTypow.Keys)
                {
                    if (klucz == Interfejsy[i])
                    {
                        CzyZawieraKlucz = true;
                    }
                }//end foreach

                if (CzyZawieraKlucz == true)
                {
                    SlownikTypow[Interfejsy[i]] = type;
                }
                else
                {
                    SlownikTypow.Add(Interfejsy[i], type);
                }
            }
        }

        /// <summary>
        /// Registering all interface implementations in assembly as providers of new instances.
        /// Resolve method will return new instance for each registered interface implementations.
        /// </summary>
        /// <param name="assembly">assembly with interface implementations</param>
        public void Register(Assembly assembly)
        {
            List<Type> TypyZawarteWAssembly = new List<Type>();

            foreach (var typ in assembly.GetTypes())
            {
                TypyZawarteWAssembly.Add(typ);
            }//end foreach

            foreach (var wiersz in TypyZawarteWAssembly)
            {
                Register(wiersz);
            }//end foreach

        }//end Register

        IDictionary<Type, object> SlownikObiektow;

        /// <summary>
        /// Registering object implementing interface T as provider of singleton.
        /// Resolve method will return exact this single instance for required interface T.
        /// </summary>
        /// <typeparam name="T">implementing interface</typeparam>
        /// <param name="impl">singleton instance</param>
        public void Register<T>(T impl) where T : class
        {
            List<Type> Interfejsy = new List<Type>();
            Type type = typeof(T);

            if (type.IsInterface)
            {
                bool CzyZawieraKlucz = false;

                foreach (var klucz in SlownikObiektow.Keys)
                {
                    if (klucz == type)
                    {
                        CzyZawieraKlucz = true;
                    }
                }//end foreach

                if (CzyZawieraKlucz == true)
                {
                    SlownikObiektow[type] = impl;
                }
                else
                {
                    SlownikObiektow.Add(type, impl);
                }
            }
            else
            {
                foreach (var interfejs in type.GetInterfaces())
                {
                    Interfejsy.Add(interfejs);
                }

                for (int i = 0; i < Interfejsy.Count; i++)
                {
                    bool CzyZawieraKlucz = false;

                    foreach (var klucz in SlownikObiektow.Keys)
                    {
                        if (klucz == Interfejsy[i])
                        {
                            CzyZawieraKlucz = true;
                        }
                    }//end foreach

                    if (CzyZawieraKlucz == true)
                    {
                        SlownikObiektow[Interfejsy[i]] = impl;
                    }
                    else
                    {
                        SlownikObiektow.Add(Interfejsy[i], impl);
                    }

                }//end for

            }//end else
        }

        IDictionary<Type, Delegate> SlownikProviderow;

        /// <summary>
        /// Registering object providing implementation of interface T.
        /// Resolve method will execute this instance of provider to obtain interface T implementation.
        /// </summary>
        /// <typeparam name="T">implementing interface</typeparam>
        /// <param name="provider">provider instance</param>
        public void Register<T>(Func<T> provider) where T : class
        {
            List<Type> Interfejsy = new List<Type>();
            Type type = typeof(T);

            if (type.IsInterface)
            {
                bool CzyZawieraKlucz = false;

                foreach (var klucz in SlownikProviderow.Keys)
                {
                    if (klucz == type)
                    {
                        CzyZawieraKlucz = true;
                    }
                }//end foreach

                if (CzyZawieraKlucz == true)
                {
                    SlownikProviderow[type] = provider;
                }
                else
                {
                    SlownikProviderow.Add(type, provider);
                }
            }

            else
            {
                foreach (var interfejs in type.GetInterfaces())
                {
                    Interfejsy.Add(interfejs);
                }

                for (int i = 0; i < Interfejsy.Count; i++)
                {
                    bool CzyZawieraKlucz = false;

                    foreach (var klucz in SlownikProviderow.Keys)
                    {
                        if (klucz == Interfejsy[i])
                        {
                            CzyZawieraKlucz = true;
                        }
                    }//end foreach

                    if (CzyZawieraKlucz == true)
                    {
                        SlownikProviderow[Interfejsy[i]] = provider;
                    }
                    else
                    {
                        SlownikProviderow.Add(Interfejsy[i], provider);
                    }

                }//end for
            }//end else
        }

        /// <summary>
        /// Resolving instance of object implementing required interface
        /// throws UnresolvedDependenciesException in case of not resolved dependencies for required component
        /// </summary>
        /// <param name="type">required interface</param>
        /// <returns>object implementing required interface T or null if no implementation registered</returns>
        public object Resolve(Type type)
        {
            if (type.IsInterface)
            {
                if (SlownikObiektow.ContainsKey(type))
                {
                    return SlownikObiektow[type];
                }//end jesli SlownikObiektow zawiera typ

                else if (SlownikProviderow.ContainsKey(type))
                {
                    return SlownikProviderow[type].DynamicInvoke();
                }//end jesli SlownikProvaiderow zawiera typ

                else if (SlownikTypow.ContainsKey(type))
                {
                    List<ConstructorInfo> ListaKonstruktorow = new List<ConstructorInfo>();
                    List<ParameterInfo[]> ListaParametrowKonstruktorow = new List<ParameterInfo[]>();
                    List<object> ListaWynikowaParametrow = new List<object>();
                    object RESOLVE = null;
                    int count = 0;

                    foreach (var KonstruktoryDlaTypu in SlownikTypow[type].GetConstructors())
                    {
                        ListaKonstruktorow.Add(KonstruktoryDlaTypu);
                        ListaParametrowKonstruktorow.Add(KonstruktoryDlaTypu.GetParameters());
                    }

                    for (int iteracja = 0; iteracja < ListaKonstruktorow.Count; iteracja++)
                    {
                        if (ListaParametrowKonstruktorow[iteracja].Count() > count)
                        {
                            //mechanizm zapobiegania powtorzeniom
                            count = ListaParametrowKonstruktorow[iteracja].Count((x) => (x.ParameterType.IsInterface == true));
                            ListaWynikowaParametrow.Clear();
                            RESOLVE = null;

                            foreach (var parametr in ListaParametrowKonstruktorow[iteracja])
                            {
                                if (parametr.ParameterType.IsInterface)
                                {
                                    RESOLVE = Resolve(parametr.ParameterType);
                                }
                                if (RESOLVE == null)
                                {
                                    UnresolvedDependenciesException e = new UnresolvedDependenciesException();
                                    throw e;
                                }
                                ListaWynikowaParametrow.Add(RESOLVE);
                            }//end foreach
                        }//end if Count
                    }//end for
                    var ObiektImplementujacy = Activator.CreateInstance(SlownikTypow[type], ListaWynikowaParametrow.ToArray());
                    return ObiektImplementujacy;
                }//end jesli SlownikTypow zawiera typ   
                else
                {
                    return null;
                }//end jesli zaden nie zawiera

            }//end if interface
            else
            {
                ArgumentException e = new ArgumentException("Podany parametr nie jest interfejsem!");
                throw e;
            }
        }//end Resolve

        /// <summary>
        /// Resolving instance of object implementing required interface
        /// throws UnresolvedDependenciesException in case of not resolved dependencies for required component
        /// </summary>
        /// <typeparam name="T">required interface</typeparam>
        /// <returns>object implementing required interface T or null if no implementation registered</returns>
        public T Resolve<T>() where T : class
        {
            Type t = typeof(T);
            T rzutowanie = Resolve(t) as T;
            return rzutowanie;
        }//end Resolve<T>


        public Container()
        {
            SlownikObiektow = new Dictionary<Type, object>();
            SlownikTypow = new Dictionary<Type, Type>();
            SlownikProviderow = new Dictionary<Type, Delegate>();
        }//end konstruktor

    }
}
