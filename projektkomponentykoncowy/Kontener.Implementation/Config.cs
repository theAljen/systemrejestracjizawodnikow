﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;
using System.Reflection;
using SystemLogowania.Implementation;
using SystemZarzadzaniaZawodami.Implementation;
using Uzytkownicy.Implementation;
using SystemLosujacy.Implementation;
using SystemKalendarza.Implementation;

namespace Kontener.Implementation
{
    public class Config
    {
        public static IContainer Configure()
        {
            IContainer cont = new Container();
            cont.Register(Assembly.GetAssembly(typeof(Formularz)));
            cont.Register(Assembly.GetAssembly(typeof(LogikaZarzadzania)));
            cont.Register(Assembly.GetAssembly(typeof(Trener)));
            cont.Register(Assembly.GetAssembly(typeof(Losowanie)));
            cont.Register(Assembly.GetAssembly(typeof(EdycjaZawodow)));
            cont.Register(Assembly.GetAssembly(typeof(Kalendarz)));
            return cont;
        }
    }
}
