﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace SystemKalendarza.Contract
{
    [ServiceContract]
    public interface IKalendarz
    {
        [OperationContract]
        List<Zawody> pobierzListeZawodow();
    }
}
