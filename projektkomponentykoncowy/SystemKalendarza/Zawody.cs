﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace SystemKalendarza.Contract
{
    [DataContract]
    public class Zawody
    {
        public Zawody(string nazwa, string termin, string opis, string dyscyplina, string limit, string id)
        {
            this.nazwa = nazwa;
            this.termin = termin;
            this.opis = opis;
            this.dyscyplina = dyscyplina;
            this.limit = limit;
            this.id = id;
        }

        [DataMember]
        public string nazwa { get; set; }
        [DataMember]
        public string termin { get; set; }
        [DataMember]
        public string opis { get; set; }
        [DataMember]
        public string dyscyplina { get; set; }
        [DataMember]
        public string limit { get; set; }
        [DataMember]
        public string id { get; set; }
    }
}
