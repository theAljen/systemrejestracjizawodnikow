﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SystemLogowania.Contract;
using SystemLogowania.Implementation;

using Ninject;
using Ninject.Extensions.Conventions;
using Ninject.Extensions.Wcf;


namespace SystemLogowania.Host
{
    class Program
    {
        public static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel();
            kernel.Bind<IUzytkownicy>().To<Formularz>();

            kernel.Bind(x => x
                .FromAssemblyContaining<Formularz>()
                .IncludingNonePublicTypes()
                .SelectAllClasses()
                .InheritedFrom<IUzytkownicy>()
                .BindAllInterfaces()
                .Configure(b => b.InSingletonScope()));

            using (var host = kernel.Get<NinjectServiceHost<Formularz>>())
            {
                host.Open();

                Console.WriteLine("HOST: Logowanie");
                Console.WriteLine("the service is ready at {0}", host.BaseAddresses[0]);
                Console.WriteLine("press <enter> to stop the service.");
                Console.ReadLine();

                host.Close();
            }

        }
    }
}
