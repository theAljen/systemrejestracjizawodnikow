﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using SystemLosujacy.Contract;

//UWAGA: Dodalem Referencje
using SystemZarzadzaniaZawodami.Implementation;
using System.IO;

namespace SystemLosujacy.Implementation
{
    public class Losowanie : ILosowanie
    {
        public int LiczbaRekordowWBazieZawodnikow(int idZawodow)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");
            int indeks = 0;


            string kwerenda = string.Format("SELECT * FROM {0}", "Zawody" + idZawodow);
            SqlCommand cmd = new SqlCommand(kwerenda, connection);

            connection.Open();
            SqlDataReader myreader = cmd.ExecuteReader();

            while (myreader.Read())
            {
                try
                {
                    indeks++;
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
            connection.Close();
            return indeks;

        }

        /// <summary>
        /// Metoda tworząca tablicę imion zawodników z tabeli "Zawody + INDEKS" bazy BazaZawodnikow.
        /// </summary>
        /// <param name="idZawodow">INDEKS zawodów/tabeli</param>
        /// <returns>Tablica imion zawodników z bazy</returns>
        public string[] StworzTabliceImion(int idZawodow)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");

            int id = 0;

            int liczbaRekorodw = LiczbaRekordowWBazieZawodnikow(idZawodow);
            string[] TablicaImion = new string[liczbaRekorodw];

            for (int i = 0; i < liczbaRekorodw; i++)
            {
                id++;

                string queryString = "SELECT * From {1} WHERE ZawodnikID ='{0}'";
                string query = String.Format(queryString, id, "Zawody" + idZawodow);
                SqlCommand cmd = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader czytacz = cmd.ExecuteReader();

                if (czytacz.Read())
                {
                    string imie = czytacz["Imie"].ToString();

                    TablicaImion[i] = imie;

                    connection.Close();
                }
                else
                {
                    connection.Close();
                }
            }
            return TablicaImion;

        }//koniec metody

        /// <summary>
        /// Metoda tworząca tablicę nazwisk zawodników z tabeli "Zawody + INDEKS" bazy BazaZawodnikow.
        /// </summary>
        /// <param name="idZawodow">INDEKS zawodów/tabeli</param>
        /// <returns>Tablica nazwisk zawodników z bazy</returns>
        public string[] StworzTabliceNazwisk(int idZawodow)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");

            int id = 0;

            int liczbaRekorodw = LiczbaRekordowWBazieZawodnikow(idZawodow);
            string[] TablicaNazwisk = new string[liczbaRekorodw];

            for (int i = 0; i < liczbaRekorodw; i++)
            {
                id++;

                string queryString = "SELECT * From {1} WHERE ZawodnikID ='{0}'";
                string query = String.Format(queryString, id, "Zawody" + idZawodow);
                SqlCommand cmd = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader czytacz = cmd.ExecuteReader();

                if (czytacz.Read())
                {
                    string nazwisko = czytacz["Nazwisko"].ToString();

                    TablicaNazwisk[i] = nazwisko;

                    connection.Close();
                }
                else
                {
                    connection.Close();
                }
            }
            return TablicaNazwisk;

        }//koniec metody


        /// <summary>
        /// Metoda tworząca tablicę wag zawodników z tabeli "Zawody + INDEKS" bazy BazaZawodnikow.
        /// </summary>
        /// <param name="idZawodow">INDEKS zawodów/tabeli</param>
        /// <returns>Tablica wag zawodników z bazy</returns>
        public string[] StworzTabliceWag(int idZawodow)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");

            int id = 0;

            int liczbaRekorodw = LiczbaRekordowWBazieZawodnikow(idZawodow);
            string[] TablicaWag = new string[liczbaRekorodw];

            for (int i = 0; i < liczbaRekorodw; i++)
            {
                id++;

                string queryString = "SELECT * From {1} WHERE ZawodnikID ='{0}'";
                string query = String.Format(queryString, id, "Zawody" + idZawodow);
                SqlCommand cmd = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader czytacz = cmd.ExecuteReader();

                if (czytacz.Read())
                {
                    string waga = czytacz["KategoriaWagowa"].ToString();

                    TablicaWag[i] = waga;

                    connection.Close();
                }
                else
                {
                    connection.Close();
                }
            }
            return TablicaWag;

        }//koniec metody


        /// <summary>
        /// Metoda tworząca tablicę kategorii wiekowych zawodników z tabeli "Zawody + INDEKS" bazy BazaZawodnikow.
        /// </summary>
        /// <param name="idZawodow">INDEKS zawodów/tabeli</param>
        /// <returns>Tablica kategorii wiekowych zawodników z bazy</returns>
        public string[] StworzTabliceWiekow(int idZawodow)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");

            int id = 0;

            int liczbaRekorodw = LiczbaRekordowWBazieZawodnikow(idZawodow);
            string[] TablicaWiekow = new string[liczbaRekorodw];

            for (int i = 0; i < liczbaRekorodw; i++)
            {
                id++;

                string queryString = "SELECT * From {1} WHERE ZawodnikID ='{0}'";
                string query = String.Format(queryString, id, "Zawody" + idZawodow);
                SqlCommand cmd = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader czytacz = cmd.ExecuteReader();

                if (czytacz.Read())
                {
                    string wiek = czytacz["KategoriaWiekowa"].ToString();

                    TablicaWiekow[i] = wiek;

                    connection.Close();
                }
                else
                {
                    connection.Close();
                }
            }
            return TablicaWiekow;

        }//koniec metody


        /// <summary>
        /// Metoda tworząca tablicę płci zawodników z tabeli "Zawody + INDEKS" bazy BazaZawodnikow.
        /// </summary>
        /// <param name="idZawodow">INDEKS zawodów/tabeli</param>
        /// <returns>Tablica płci zawodników z bazy</returns>
        public string[] StworzTablicePlci(int idZawodow)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");

            int id = 0;

            int liczbaRekorodw = LiczbaRekordowWBazieZawodnikow(idZawodow);
            string[] TablicaPlci = new string[liczbaRekorodw];

            for (int i = 0; i < liczbaRekorodw; i++)
            {
                id++;

                string queryString = "SELECT * From {1} WHERE ZawodnikID ='{0}'";
                string query = String.Format(queryString, id, "Zawody" + idZawodow);
                SqlCommand cmd = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader czytacz = cmd.ExecuteReader();

                if (czytacz.Read())
                {
                    string plec = czytacz["Plec"].ToString();

                    TablicaPlci[i] = plec;

                    connection.Close();
                }
                else
                {
                    connection.Close();
                }
            }
            return TablicaPlci;

        }//koniec metody

        /// <summary>
        /// Metoda wykonująca losowanie rozstawienia zawodników.
        /// </summary>
        /// <param name="idZawodow">ID zawodów, w których losowanie chcemy wykonać.</param>
        /// <param name="waga">Kategoria wagowa zawodników, dla której losowanie chcemy wykonać.</param>
        /// <param name="wiek">Kategoria wiekowa zawodników, dla której losowanie chcemy wykonać. Możliwości: SNR, JNR lub KDT.</param>
        /// <param name="plec">Płeć zawodników, dla której losowanie chcemy wykonać. Możliwości: M lub K.</param>
        /// <returns>Tablica imion i nazwisk zawodnikow, spełniajacych podane kryteria o losowej kolejności zawodników.</returns>
        public string[] Losuj(int idZawodow, int waga, string wiek, string plec)
        {
            int iloscZawodnikow = LiczbaRekordowWBazieZawodnikow(idZawodow);


            List<string> ListaZawodnikow = new List<string>();
            List<string> ListaImionZawodnikow = new List<string>();

            string[] BazaNazwiskZawodnikow = StworzTabliceNazwisk(idZawodow);
            string[] BazaImionZawodnikow = StworzTabliceImion(idZawodow);
            string[] BazaWagZawodnikow = StworzTabliceWag(idZawodow);
            string[] BazaWiekowZawodnikow = StworzTabliceWiekow(idZawodow);
            string[] BazaPlciZawodnikow = StworzTablicePlci(idZawodow);

            for (int i = 0; i < iloscZawodnikow; i++)
            {
                if ((BazaWagZawodnikow[i] == Convert.ToString(waga)) && (BazaWiekowZawodnikow[i] == wiek) && (BazaPlciZawodnikow[i] == plec))
                {
                    ListaZawodnikow.Add(BazaNazwiskZawodnikow[i]);
                    ListaImionZawodnikow.Add(BazaImionZawodnikow[i]);
                }
            }

            string[] ZapamietanaTablicaLosowania = new string[ListaZawodnikow.Count()];

            Random losowa = new Random();

            int ilosc = ListaZawodnikow.Count;

            for (int i = 0; i < ilosc; i++)
            {
                int wylosowana = losowa.Next(0, ListaZawodnikow.Count);

                ZapamietanaTablicaLosowania[i] = ListaZawodnikow[wylosowana] + " " + ListaImionZawodnikow[wylosowana];

                ListaZawodnikow.RemoveAt(wylosowana);
                ListaImionZawodnikow.RemoveAt(wylosowana);
            }

            return ZapamietanaTablicaLosowania;

        }

        /// <summary>
        /// Metoda umieszczająca wykonane wcześniej losowanie w bazie danych. Dzięki niej wyniki losowań zapisują się po zamknięciu programu.
        /// </summary>
        /// <param name="losowanie">Tablica imion i nazwisk zawodników, ustawionych w odpowiedniej kolejności.</param>
        /// <param name="IDzawodow">ID zawodów, których losowanie chcemy umiescić w bazie.</param>
        /// <param name="waga">Kategoria wagowa zawodników, których chcemy umieścić w bazie.</param>
        /// <param name="wiek">Kategoria wiekowa zawodników, dla której losowanie chcemy wykonać. Możliwości: SNR, JNR lub KDT.</param>
        /// <param name="plec">Płeć zawodników, dla której losowanie chcemy wykonać. Możliwości: M lub K.</param>
        public void UmiescRozlosowanychZawodnikowWBazie(string[] losowanie, int IDzawodow, int waga, string wiek, string plec)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaLosowan.mdf" + @";Integrated Security=True");

            string query;
            SqlCommand cmd;

            int dlugoscTab = losowanie.Count();

            string nazwaTabeli = "Zawody" + Convert.ToString(IDzawodow) + plec + "Kat" + Convert.ToString(waga) + wiek;

            if (LiczbaRekordowWLosowaniu(nazwaTabeli) == 0)
            {

                for (int i = 0; i < dlugoscTab; i++)
                {
                    Polaczenie.Open();

                    query = String.Format("INSERT INTO {0} ([Pozycja], [ImieNazwisko]) VALUES (@Pozycja, @NazwaZawodnika)", nazwaTabeli);
                    cmd = new SqlCommand(query, Polaczenie);

                    cmd.Parameters.AddWithValue("@Pozycja", i + 1);
                    cmd.Parameters.AddWithValue("@NazwaZawodnika", losowanie[i]);

                    cmd.ExecuteNonQuery();

                    Polaczenie.Close();
                }
                MessageBox.Show("Losowanie przebieglo pomyslnie");
            }
            else
            {
                MessageBox.Show("Nie mozna ponownie przeprowadzic losowania");
            }
        }

        /// <summary>
        /// Metoda zliczająca ilość rekordów w konkretnym losowaniu.
        /// </summary>
        /// <param name="nazwaLosowania">Nazwa tabeli z losowaniem, znajdujacej się w bazie BazaLosowań.mdf.</param>
        /// <returns>Liczba zawodników w konkretnej drabince.</returns>
        public int LiczbaRekordowWLosowaniu(string nazwaLosowania)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaLosowan.mdf" + @";Integrated Security=True");
            int indeks = 0;

            string kwerenda = string.Format("SELECT * FROM {0}", nazwaLosowania);
            SqlCommand cmd = new SqlCommand(kwerenda, connection);

            connection.Open();
            SqlDataReader myreader = cmd.ExecuteReader();

            while (myreader.Read())
            {
                try
                {
                    indeks++;
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
            connection.Close();
            return indeks;

        }
        public void ZamienMiejscami(string zawody, string zawodnikPierwszy, string zawodnikDrugi, int indexZawodnikPierwszy, int indexZawodnikDrugi)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaLosowan.mdf" + @";Integrated Security=True");
            string queryString = "UPDATE {0} SET [ImieNazwisko]='{1}' WHERE Pozycja = '{2}'";
            string querry1 = String.Format(queryString, zawody, zawodnikPierwszy, indexZawodnikDrugi);
            string querry2 = String.Format(queryString, zawody, zawodnikDrugi, indexZawodnikPierwszy);
            SqlCommand abc1;
            SqlCommand abc2;

            try
            {
                abc1 = new SqlCommand(querry1);
                abc2 = new SqlCommand(querry2);

                Polaczenie.Open();

                abc1.Connection = Polaczenie;
                abc1.ExecuteNonQuery();
                abc2.Connection = Polaczenie;
                abc2.ExecuteNonQuery();

                Polaczenie.Close();
                MessageBox.Show("Zamiana przebiegła pomyślnie!");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
