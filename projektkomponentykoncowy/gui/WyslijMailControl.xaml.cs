﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

using System.Data.SqlClient;
using WysylanieMaili.Kontrakt;

namespace GUI
{
    /// <summary>
    /// Interaction logic for WyslijMailControl.xaml
    /// </summary>
    public partial class WyslijMailControl : UserControl
    {
        List<string> adresy = new List<string>();
        IOperacjeMail wyslijMail = new OperacjeMail();

        public WyslijMailControl()
        {
            InitializeComponent();

            //Uzupelnianie ComboBoxa zawodami:
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodow.mdf" + @";Integrated Security=True");
            String queryString = "SELECT * From Zawody ";
            String query = String.Format(queryString);
            SqlCommand abc = new SqlCommand(query, Polaczenie);
            Polaczenie.Open();
            SqlDataReader myreader = abc.ExecuteReader();
            while (myreader.Read())
            {
                string imie = myreader["Nazwa"].ToString();
                Zawody.Items.Add(imie);
            }
        }

        private void Akceptacja_Click(object sender, RoutedEventArgs e)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");

            int wybraneZawody = Zawody.SelectedIndex + 1;

            String queryStringg = "SELECT * From {0}";
            String queryy = String.Format(queryStringg, "Zawody" + Convert.ToString(wybraneZawody));

            SqlCommand abcd = new SqlCommand(queryy, Polaczenie);
            SqlDataReader myreaderr;

            try
            {
                Polaczenie.Open();
                myreaderr = abcd.ExecuteReader();

                while (myreaderr.Read())
                {

                    string mail = myreaderr["Mail"].ToString();

                    if (adresy.Contains(mail) == false)
                    {
                        adresy.Add(mail);
                    }


                }
            }
            catch (Exception exx)
            {

                MessageBox.Show("Błąd zczytywania adresów e-mail " + exx);
            }

        }

        private void Wyslij_Click(object sender, RoutedEventArgs e)
        {
            string temat = "brak tematu";
            string tresc = "brak tresci";

            if (Temat.Text != "")
            {
                temat = Temat.Text;
            }

            if (Tresc.Text != "")
            {
                tresc = Tresc.Text;
            }

            if (adresy.Count != 0)
            {
                try
                {
                    wyslijMail.SeryjnyMail("systemrejestracjiSGGW@gmail.com", "123Banana", "smtp.gmail.com", 587, adresy, temat, tresc);
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }
    }
}