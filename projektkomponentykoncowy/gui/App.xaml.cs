﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using SystemLogowania.Contract;
using Kontener.Implementation;
using GUI.ViewModels;
using SystemZarzadzaniaZawodami.Contract;
using Uzytkownicy.Contract;
using SystemLosujacy.Contract;
using Ninject;
using System.ServiceModel;
using SystemKalendarza.Contract;

namespace GUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void AppStart(object sender, StartupEventArgs e)
        {
            IKernel kernel = new StandardKernel();
            kernel.Bind<IUzytkownicy>().ToMethod(ctx =>
            {
                var factory = new ChannelFactory<IUzytkownicy>("systemLogowania");
                return factory.CreateChannel();
            });
            kernel.Bind<IKalendarz>().ToMethod(ctx =>
            {
                var factory = new ChannelFactory<IKalendarz>("systemKalendarza");
                return factory.CreateChannel();
            });
            var container = Config.Configure();
            var uzytkownicy = container.Resolve<IUzytkownicy>();
            var logika = container.Resolve<IAdmin>();
            var trener = container.Resolve<ITrener>();
            var losowanie = container.Resolve<ILosowanie>();
            var edytorZawodnika = container.Resolve<IZawody>();
            var iuzytkownicy = kernel.Get<IUzytkownicy>();
            var ikalendarz = kernel.Get<IKalendarz>();
            MainWindow mainwindow = new MainWindow();
            Interfejs i = new Interfejs(uzytkownicy, mainwindow, logika, trener, losowanie, edytorZawodnika, iuzytkownicy, ikalendarz);
        }
    }
}
