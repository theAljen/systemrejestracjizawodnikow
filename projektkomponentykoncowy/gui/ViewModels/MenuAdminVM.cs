﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace GUI.ViewModels
{
    class MenuAdminVM : ViewModel
    {
        private Command se;
        private Command ee;
        private Command ki;
        private Command zz;
        private Command ez;
        private Command ol;
        private Command el;
        private Command email;
        private Command wyjscie;

        public Command Se
        {
            get { return se; }
        }
        public Command Ee
        {
            get { return ee; }
        }
        public Command Ki
        {
            get { return ki; }
        }
        public Command Zz
        {
            get { return zz; }
        }
        public Command Ez
        {
            get { return ez; }
        }
        public Command Ol
        {
            get { return ol; }
        }
        public Command Email
        {
            get { return email; }
        }
        public Command Wyjscie
        {
            get { return wyjscie; }
        }
        public Command El
        {
            get { return el; }
        }
        
        public MenuAdminVM(Command se, Command ee, Command ki, Command zz, Command ez, Command ol, Command el, Command email, Command wyjscie)
        {
            this.se = se;
            this.ee = ee;
            this.ki = ki;
            this.zz = zz;
            this.ez = ez;
            this.ol = ol;
            this.el = el;
            this.email = email;
            this.wyjscie = wyjscie;
        }
    }
}
