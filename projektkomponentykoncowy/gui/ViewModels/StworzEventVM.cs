﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace GUI.ViewModels
{
    class StworzEventVM : ViewModel
    {
        private Command powrot;
        private Command czysc;
        private Command zapisz;

        public Command Powrot
        {
            get { return powrot; }
        }
        public Command Czysc
        {
            get { return czysc; }
        }
        public Command Zapisz
        {
            get { return zapisz; }
        }

        public StworzEventVM(Command powrot, Command czysc, Command zapisz)
        {
            this.powrot = powrot;
            this.czysc = czysc;
            this.zapisz = zapisz;
        }
    }
}
