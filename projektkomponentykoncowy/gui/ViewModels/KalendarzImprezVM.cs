﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace GUI.ViewModels
{
    class KalendarzImprezVM : ViewModel
    {
        private Command wyjscie;
        private Command powrot;
        private Command drabinka;

        public Command Wyjscie
        {
            get { return wyjscie; }
        }
        public Command Powrot
        {
            get { return powrot; }
        }
        public Command Drabinka
        {
            get { return drabinka; }
        }

        public KalendarzImprezVM(Command wyjscie, Command powrot, Command drabinka)
        {
            this.wyjscie = wyjscie;
            this.powrot = powrot;
            this.drabinka = drabinka;
        }
    }
}
