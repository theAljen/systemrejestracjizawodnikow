﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper; 

namespace GUI.ViewModels
{
    class RejestracjaZawodnikaVM : ViewModel
    {
        private Command powrot;
        private Command wyjscie;
        private Command wyczysc;
        private Command zapisz;
        private Command akceptuj;

        public Command Powrot
        {
            get { return powrot; }
        }
        public Command Wyjscie
        {
            get { return wyjscie; }
        }
        public Command Wyczysc
        {
            get { return wyczysc; }
        }
        public Command Zapisz
        {
            get { return zapisz; }
        }
        public Command Akceptuj
        {
            get { return akceptuj; }
        }

        public RejestracjaZawodnikaVM(Command powrot, Command wyjscie, Command wyczysc, Command zapisz, Command akceptuj)
        {
            this.powrot = powrot;
            this.wyjscie = wyjscie;
            this.wyczysc = wyczysc;
            this.zapisz = zapisz;
            this.akceptuj = akceptuj;
        }
    }
}
