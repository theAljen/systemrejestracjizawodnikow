﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace GUI.ViewModels
{
    class LosowanieAdminVM : ViewModel
    {
        private Command powrot;
        private Command start;

        public Command Powrot
        {
            get { return powrot; }
        }
        public Command Start
        {
            get { return start; }
        }

        public LosowanieAdminVM(Command powrot, Command start)
        {
            this.powrot = powrot;
            this.start = start;
        }
    }
}
