﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace GUI.ViewModels
{
    class LogowanieVM : ViewModel
    {
        private Command cmd;
        private Command cmd2;
        private Command autorzy;
        private Command wyjscie;

        public Command Cmd
        {
            get { return cmd; }
        }
        public Command Cmd2
        {
            get { return cmd2; }
        }
        public Command Autorzy
        {
            get { return autorzy; }
        }
        public Command Wyjscie
        {
            get { return wyjscie; }
        }

        public LogowanieVM(Command cmd, Command cmd2, Command autorzy, Command wyjscie)
        {
            this.cmd = cmd;
            this.cmd2 = cmd2;
            this.autorzy = autorzy;
            this.wyjscie = wyjscie;
        }

        private string login;
        public string Login
        {
            get { return login; }
            set
            {
                login = value;
                FirePropertyChanged("login");
            }
        }
        private string pass;
        public string Pass
        {
            get { return pass; }
            set
            {
                pass = value;
                FirePropertyChanged("pass");
            }
        }
    }
}
