﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace GUI.ViewModels
{
    class EdytujLosowanieVM : ViewModel
    {
        private Command powrot;
        private Command zamien;

        public Command Powrot
        {
            get { return powrot; }
        }
        public Command Zamien
        {
            get { return zamien; }
        }

        public EdytujLosowanieVM(Command powrot, Command zamien)
        {
            this.powrot = powrot;
            this.zamien = zamien;
        }
    }
}
