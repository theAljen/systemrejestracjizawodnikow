﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace GUI.ViewModels
{
    class MenuTrenerVM : ViewModel
    {
        private Command pd;
        private Command ki;
        private Command zz;
        private Command wyjscie;

        public Command Pd
        {
            get { return pd; }
        }
        public Command Ki
        {
            get { return ki; }
        }
        public Command Zz
        {
            get { return zz; }
        }
        public Command Wyjscie
        {
            get { return wyjscie; }
        }
        public MenuTrenerVM(Command pd, Command ki, Command zz, Command wyjscie)
        {
            this.pd = pd;
            this.ki = ki;
            this.zz = zz;
            this.wyjscie = wyjscie;
        }
    }
}
