﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace GUI.ViewModels
{
    class EdytujEventVM : ViewModel
    {
        private Command powrot;
        private Command wyczysc;
        private Command edytuj;

        public Command Powrot
        {
            get { return powrot; }
        }
        public Command Wyczysc
        {
            get { return wyczysc; }
        }
        public Command Edytuj
        {
            get { return edytuj; }
        }

        public EdytujEventVM(Command powrot, Command wyczysc, Command edytuj)
        {
            this.powrot = powrot;
            this.wyczysc = wyczysc;
            this.edytuj = edytuj;
        }
    }
}
