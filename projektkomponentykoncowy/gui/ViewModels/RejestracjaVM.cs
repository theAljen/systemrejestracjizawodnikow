﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace GUI.ViewModels
{
    class RejestracjaVM : ViewModel
    {
        private Command cmd;
        private Command autorzy;
        private Command wyjscie;
        private Command powrot;

        public Command Cmd
        {
            get { return cmd; }
        }
        public Command Autorzy
        {
            get { return autorzy; }
        }
        public Command Wyjscie
        {
            get { return wyjscie; }
        }
        public Command Powrot
        {
            get { return powrot; }
        }

        public RejestracjaVM(Command cmd, Command autorzy, Command wyjscie, Command powrot)
        {
            this.cmd = cmd;
            this.autorzy = autorzy;
            this.wyjscie = wyjscie;
            this.powrot = powrot;
        }


        private string login;
        public string Login
        {
            get { return login; }
            set
            {
                login = value;
                FirePropertyChanged("login");
            }
        }
        private string pass;
        public string Pass
        {
            get { return pass; }
            set
            {
                pass = value;
                FirePropertyChanged("pass");
            }
        }
    }
}
