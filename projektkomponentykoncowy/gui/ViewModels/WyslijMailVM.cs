﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace GUI.ViewModels
{
    class WyslijMailVM : ViewModel
    {
        private Command powrot;

        public Command Powrot
        {
            get { return powrot; }
        }

        public WyslijMailVM(Command powrot)
        {
            this.powrot = powrot;
        }
    }
}
