﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;
using SystemLogowania.Contract;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using SystemZarzadzaniaZawodami.Contract;
using Uzytkownicy.Contract;
using SystemLosujacy.Contract;
using SystemKalendarza.Contract;

namespace GUI.ViewModels
{
    class Interfejs
    {
        IUzytkownicy IU;
        IUzytkownicy IUSOA;
        IAdmin IA;
        ITrener IT;
        ILosowanie IL;
        IZawody IZ;
        IKalendarz IK;

        Drabinka drabinka;
        DrabinkaVM drabinkaVM;
        LogowanieControl LC;
        LogowanieVM LVM;
        RejestracjaControl RC;
        RejestracjaVM RVM;
        MainWindow MW;
        MenuAdminControl MAC;
        MenuAdminVM MAVM;
        MenuTrenerControl MTC;
        MenuTrenerVM MTVM;
        
        StworzEventAdminControl SEAC;
        StworzEventVM SEVM;
        EdytujEventAdminControl EEAC;
        EdytujEventVM EEVM;
        KalendarzImprez KI;
        KalendarzImprezVM KIVM;
        EdytujLosowanieAdminControl ELAC;
        EdytujLosowanieVM ELVM;
        RejestracjaZawodnikaControl RZC;
        RejestracjaZawodnikaVM RZVM;
        EdytujZawodnikaAdmin EZAC;
        EdytujZawodnikaVM EZVM;
        LosowanieAdminControl LAC;
        LosowanieAdminVM LAVM;
        WyslijMailControl WMC;
        WyslijMailVM WMVM;

        public Interfejs(IUzytkownicy IU, MainWindow MW, IAdmin IA, ITrener IT, ILosowanie IL, IZawody IZ, IUzytkownicy IUSOA, IKalendarz IK)
        {
            this.IU = IU;
            this.MW = MW;
            this.IA = IA;
            this.IT = IT;
            this.IL = IL;
            this.IZ = IZ;
            this.IUSOA = IUSOA;
            this.IK = IK;

            LC = new LogowanieControl();
            LVM = new LogowanieVM(new Command(Login), new Command(Rejestracja_uzytkownika), new Command(Autorzy), new Command(Zamknij));
            LC.DataContext = LVM;

            MAC = new MenuAdminControl();
            MAVM = new MenuAdminVM(new Command(Stwórz_event), new Command(Edycja_zawodow), new Command(Kalendarz_imprez), new Command(Rejestracja_zawodnika), new Command(Edycja_zawodnika),
                new Command(Okno_losowania), new Command(Edycja_losowania),new Command(Wyslij_mail), new Command(Zamknij));
            MAC.DataContext = MAVM;

            MTC = new MenuTrenerControl();
            MTVM = new MenuTrenerVM(new Command(Pokaz_drabinke), new Command(Kalendarz_imprez), new Command(Rejestracja_zawodnika), new Command(Zamknij));
            MTC.DataContext = MTVM;

            MW.grid.Children.Add(LC);
            MW.Show();
        }
        private void Login(object parameter)
        {
            string llogin = LVM.Login;
            string hhaslo = LVM.Pass;
            
            LogowanieOperation logowanieOperation = IUSOA.Zaloguj(llogin, hhaslo);

            if(logowanieOperation.Zalogowano)
            {
                string nazwa = logowanieOperation.RodzajUprawnien;

                if (nazwa == "jeden")
                {
                    MW.grid.Children.Clear();
                    MW.grid.Children.Add(MAC);
                }
                else if (nazwa == "dwa")
                {
                    MW.grid.Children.Clear();
                    MW.grid.Children.Add(MTC);
                }
                else if (nazwa == "trzy")
                {
                    List<Zawody> zawodyList = IK.pobierzListeZawodow();
                    KI = new KalendarzImprez(zawodyList);
                    KIVM = new KalendarzImprezVM(new Command(Zamknij), new Command(Zamknij), new Command(Powrót));
                    KI.DataContext = KIVM;
                    MW.grid.Children.Clear();
                    MW.grid.Children.Add(KI);
                    KI.Powrot.Visibility = System.Windows.Visibility.Hidden;
                }
            }
            else
            {
                MessageBox.Show("Nieprawidlowe dane logowania!");
            }

        }
        private void Rejestracja_uzytkownika(object parameter)
        {
            RC = new RejestracjaControl();
            RVM = new RejestracjaVM(new Command(Register), new Command(Autorzy), new Command(Zamknij), new Command(Powrót));
            RC.DataContext = RVM;
            LC.Visibility = System.Windows.Visibility.Hidden;
            MW.grid.Children.Add(RC);
        }

        private void Register(object parameter)
        {
            string login = RVM.Login;
            string haslo = RVM.Pass;
            string uprawnienia = "trzy";

            RVM.Login = "";
            RVM.Pass = "";

            try
            {
                IUSOA.Zarejestruj(login, haslo, uprawnienia);
                MW.grid.Children.Clear();
                MW.grid.Children.Add(LC);
                LC.Visibility = System.Windows.Visibility.Visible;
                MessageBox.Show("Gratulacje! Zarejestrowano pomyslnie!");
            }
            catch
            {
                MessageBox.Show("Nie udało się zarejestrować! Login już istnieje lub przynamniej jedno z pól zostało puste.");
            }
        }

        private void Stwórz_event(object parameter)
        {
            SEAC = new StworzEventAdminControl();
            SEVM = new StworzEventVM(new Command(Powrót), new Command(Stwórz_event_czyść), new Command(Dodaj_zawody));
            SEAC.DataContext = SEVM; 
            MAC.Visibility = System.Windows.Visibility.Hidden;
            MW.grid.Children.Add(SEAC);
        }
        private void Stwórz_event_czyść(object parameter)
        {
            MW.grid.Children.RemoveAt(MW.grid.Children.Count - 1);
            SEAC = new StworzEventAdminControl();
            SEAC.DataContext = SEVM;
            MW.grid.Children.Add(SEAC);
        }

        private void Edycja_zawodow(object parameter)
        {
            EEAC = new EdytujEventAdminControl();
            EEVM = new EdytujEventVM(new Command(Powrót), new Command(Edycja_zawodow_czyść), new Command(EdytujZawody));
            EEAC.DataContext = EEVM;
            MAC.Visibility = System.Windows.Visibility.Hidden;
            MW.grid.Children.Add(EEAC);
        }
        private void Edycja_zawodow_czyść(object parameter)
        {
            MW.grid.Children.RemoveAt(MW.grid.Children.Count - 1);
            EEAC = new EdytujEventAdminControl();
            EEAC.DataContext = EEVM;
            MW.grid.Children.Add(EEAC);
        }
        private void Kalendarz_imprez(object parameter)
        {
            List<Zawody> zawodyList = IK.pobierzListeZawodow();
            KI = new KalendarzImprez(zawodyList);
            KIVM = new KalendarzImprezVM(new Command(Zamknij), new Command(Powrót), new Command(Wyswietl_drabinke));
            KI.DataContext = KIVM;
            MAC.Visibility = System.Windows.Visibility.Hidden;
            MTC.Visibility = System.Windows.Visibility.Hidden;
            MW.grid.Children.Add(KI);
        }
        private void Rejestracja_zawodnika(object parameter)
        {
            RZC = new RejestracjaZawodnikaControl();
            RZVM = new RejestracjaZawodnikaVM(new Command(Powrót), new Command(Zamknij), new Command(Rejestracja_zawodnika_czyść), new Command(ZarejestrujZawodnika), new Command(RejestracjaZawodnikaComboBox));
            RZC.DataContext = RZVM;
            MAC.Visibility = System.Windows.Visibility.Hidden;
            MTC.Visibility = System.Windows.Visibility.Hidden;
            MW.grid.Children.Add(RZC);
        }
        private void Rejestracja_zawodnika_czyść(object parameter)
        {
            MW.grid.Children.RemoveAt(MW.grid.Children.Count - 1);
            RZC = new RejestracjaZawodnikaControl();
            RZC.DataContext = RZVM;
            MW.grid.Children.Add(RZC);
        }
        private void Edycja_zawodnika(object parameter)
        {
            EZAC = new EdytujZawodnikaAdmin();
            EZVM = new EdytujZawodnikaVM(new Command(Powrót), new Command(Zamknij), new Command(Edycja_zawodnika_czyść), new Command(EdytujZawodnika));
            EZAC.DataContext = EZVM; 
            MAC.Visibility = System.Windows.Visibility.Hidden;
            MW.grid.Children.Add(EZAC);
        }
        private void Edycja_zawodnika_czyść(object parameter)
        {
            MW.grid.Children.RemoveAt(MW.grid.Children.Count - 1);
            EZAC = new EdytujZawodnikaAdmin();
            EZAC.DataContext = EZVM;
            MW.grid.Children.Add(EZAC);
        }
        private void Okno_losowania(object parameter)
        {
            LAC = new LosowanieAdminControl();
            LAVM = new LosowanieAdminVM(new Command(Powrót), new Command(Losowanie));
            LAC.DataContext = LAVM;
            MAC.Visibility = System.Windows.Visibility.Hidden;
            MW.grid.Children.Add(LAC);
        }
        private void Edycja_losowania(object parameter)
        {
            ELAC = new EdytujLosowanieAdminControl();
            ELVM = new EdytujLosowanieVM(new Command(Powrót), new Command(Zamien_miejscami));
            ELAC.DataContext = ELVM;
            MAC.Visibility = System.Windows.Visibility.Hidden;
            MW.grid.Children.Add(ELAC);
        }
        private void Pokaz_drabinke(object parameter)
        {
            MAC.Visibility = System.Windows.Visibility.Hidden;
            MTC.Visibility = System.Windows.Visibility.Hidden;
            MW.grid.Children.Add(drabinka);
        }
        private void Wyslij_mail(object parameter)
        {
            WMC = new WyslijMailControl();
            WMVM = new WyslijMailVM(new Command(Powrót));
            WMC.DataContext = WMVM; 
            MAC.Visibility = System.Windows.Visibility.Hidden;
            MW.grid.Children.Add(WMC);
        }

        private void Autorzy(object parameter)
        {
            IUSOA.PobierzAutorow();
        }
        private void Zamknij(object parameter)
        {
            MW.grid.Children.Clear();
            for (int intCounter = App.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
                App.Current.Windows[intCounter].Close();
        }
        private void Powrót(object parameter)
        {
            if (MW.grid.Children.Count > 2)
            {
                MW.grid.Children.RemoveAt(MW.grid.Children.Count - 1);
                KI.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                MW.grid.Children.RemoveAt(MW.grid.Children.Count - 1);
                MAC.Visibility = System.Windows.Visibility.Visible;
                MTC.Visibility = System.Windows.Visibility.Visible;
                LC.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void Wyswietl_drabinke(object parameter)
        {
            try
            {
                var waga = Convert.ToInt32(KI.PokazWage.SelectedItem);
                var wiek = KI.PokazWiek.SelectedItem;
                var plec = KI.Pokaz.SelectedItem;
                var zawody = KI.Zawody.SelectedIndex + 1;

                string wiekS = wiek as String;
                string plecS = plec as String;

                drabinka = new Drabinka(wiekS, plecS, zawody, waga);
                drabinkaVM = new DrabinkaVM(new Command(Powrót));
                drabinka.DataContext = drabinkaVM;


                KI.Visibility = System.Windows.Visibility.Hidden;
                MW.grid.Children.Add(drabinka);
            }
            catch
            {
                MessageBox.Show("Najpierw należy wybrać wartości dotyczące zawodów, których drabinkę chcesz wyświetlić.");
            }
        }

        private void Dodaj_zawody(object parameter)
        {
            try
            {
                string nazwa = SEAC.Nazwa.Text;
                string termin = SEAC.Termin.Text;
                string opis = SEAC.Opis.Text;
                string limit = SEAC.Limit.Text;

                string plec = (SEAC.kobiety.IsChecked == true) ? "0" : "1";
                string dyscyplina = (SEAC.boks.IsChecked == true) ? "0" : ((SEAC.kickboxing.IsChecked == true) ? "1" : "2");
                string Czy54 = (SEAC.KatWag54.IsChecked == true) ? "1" : "0";
                string Czy57 = (SEAC.KatWag57.IsChecked == true) ? "1" : "0";
                string Czy63 = (SEAC.KatWag63.IsChecked == true) ? "1" : "0";
                string Czy69 = (SEAC.KatWag69.IsChecked == true) ? "1" : "0";
                string Czy74 = (SEAC.KatWag74.IsChecked == true) ? "1" : "0";
                string Czy79 = (SEAC.KatWag79.IsChecked == true) ? "1" : "0";
                string Czy84 = (SEAC.KatWag84.IsChecked == true) ? "1" : "0";
                string Czy89 = (SEAC.KatWag89.IsChecked == true) ? "1" : "0";
                string Czy94 = (SEAC.KatWag94.IsChecked == true) ? "1" : "0";
                string CzyJunior = (SEAC.Junior.IsChecked == true) ? "1" : "0";
                string CzyKadet = (SEAC.Kadet.IsChecked == true) ? "1" : "0";
                string CzySenior = (SEAC.Senior.IsChecked == true) ? "1" : "0";

                IA.DodajZawody(nazwa, termin, opis, limit, plec, dyscyplina, Czy54, Czy57, Czy63, Czy69, Czy74, Czy79, Czy84, Czy89, Czy94, CzyJunior, CzyKadet, CzySenior);
            }
            catch
            {
                MessageBox.Show("Tworzenie zawodów nie powiodło się!");
            }
        }

        private void ZarejestrujZawodnika(object parameter)
        {
            try
            {
                string imie = RZC.Imie.Text;
                string nazwisko = RZC.Nazwisko.Text;
                string plec = (RZC.kobiety.IsChecked == true) ? "K" : "M";
                string katWiekowa = RZC.KategoriaWiekowa.SelectedItem.ToString();
                string katWagowa = RZC.KategoriaWagowa.SelectedItem.ToString();
                string badania = (RZC.Tak.IsChecked == true) ? "1" : "0";
                string licencja = RZC.NumerLicencji.Text;
                string kraj = RZC.Kraj.Text;
                string klub = RZC.Klub.Text;
                string mail = RZC.Mail.Text;
                int dodawanie = RZC.Zawody.SelectedIndex + 1;

                IT.DodajZawodnika(dodawanie, imie, nazwisko, plec, katWiekowa, katWagowa, badania, licencja, kraj, klub, mail);
            }
            catch
            {
                MessageBox.Show("Rejestracja zawodnika nie powiodła się. Być może nie wszystkie pola są wypełnione.");
            }
        }

        private void RejestracjaZawodnikaComboBox(object parameter)
        {
            try
            {
                RZC.KategoriaWagowa.Items.Clear();
                RZC.KategoriaWiekowa.Items.Clear();
                int JakieZawody = RZC.Zawody.SelectedIndex + 1;
                //string path = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
                //var directory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
                string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                directory = directory.Substring(0, directory.Length - 14);
                SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodow.mdf" + @";Integrated Security=True");
                String queryString = "SELECT * From {0} WHERE ZawodyID ='{1}'";
                String query = String.Format(queryString, "Zawody", JakieZawody);
                SqlCommand abc = new SqlCommand(query, Polaczenie);
                Polaczenie.Open();
                SqlDataReader myreaderr = abc.ExecuteReader();
                if (myreaderr.Read())
                {
                    string Waga54 = myreaderr["Czy54"].ToString();
                    if (Waga54.Equals("True"))
                    {
                        RZC.KategoriaWagowa.Items.Add("54");
                    }

                    string Waga57 = myreaderr["Czy57"].ToString();
                    if (Waga57.Equals("True"))
                    {
                        RZC.KategoriaWagowa.Items.Add("57");
                    }

                    string Waga63 = myreaderr["Czy63"].ToString();
                    if (Waga63.Equals("True"))
                    {
                        RZC.KategoriaWagowa.Items.Add("63");
                    }

                    string Waga69 = myreaderr["Czy69"].ToString();
                    if (Waga69.Equals("True"))
                    {
                        RZC.KategoriaWagowa.Items.Add("69");
                    }

                    string Waga74 = myreaderr["Czy74"].ToString();
                    if (Waga74.Equals("True"))
                    {
                        RZC.KategoriaWagowa.Items.Add("74");
                    }

                    string Waga79 = myreaderr["Czy79"].ToString();
                    if (Waga79.Equals("True"))
                    {
                        RZC.KategoriaWagowa.Items.Add("79");
                    }

                    string Waga84 = myreaderr["Czy84"].ToString();
                    if (Waga84.Equals("True"))
                    {
                        RZC.KategoriaWagowa.Items.Add("84");
                    }

                    string Waga89 = myreaderr["Czy89"].ToString();
                    if (Waga89.Equals("True"))
                    {
                        RZC.KategoriaWagowa.Items.Add("89");
                    }

                    string Waga94 = myreaderr["Czy94"].ToString();
                    if (Waga94.Equals("True"))
                    {
                        RZC.KategoriaWagowa.Items.Add("94");
                    }
                    string CzySenior = myreaderr["CzySenior"].ToString();
                    if (CzySenior.Equals("True"))
                    {
                        RZC.KategoriaWiekowa.Items.Add("SNR");
                    }

                    string CzyKadet = myreaderr["CzyKadet"].ToString();
                    if (CzyKadet.Equals("True"))
                    {
                        RZC.KategoriaWiekowa.Items.Add("KDT");
                    }

                    string CzyJunior = myreaderr["CzyJunior"].ToString();
                    if (CzyJunior.Equals("True"))
                    {
                        RZC.KategoriaWiekowa.Items.Add("JNR");
                    }
                    {
                        Polaczenie.Close();
                    }

                }
                else
                {
                    Polaczenie.Close();
                }
            }
            catch
            {
                MessageBox.Show("Wybierz zawody z listy po lewej.");
            }
        }

        private void Losowanie(object parameter)
        {
            try
            {
                int waga = Convert.ToInt32(LAC.PokazWage.SelectedItem);
                string wiek = LAC.PokazWiek.SelectedItem.ToString();
                string plec = LAC.Pokaz.SelectedItem.ToString();
                int zawody = LAC.Zawody.SelectedIndex + 1;

                IL.UmiescRozlosowanychZawodnikowWBazie(IL.Losuj(zawody, waga, wiek, plec), zawody, waga, wiek, plec);

            }
            catch
            {
                MessageBox.Show("Nie udało się przeprowadzić losowania. Proszę wybrać wszystkie powyższe parametry.");
            }
        }

        private void Zamien_miejscami(object parameter)
        {
            try { 
                string zawodnikPierwszy = ELAC.ZawodnikA.SelectedItem.ToString();
                string zawodnikDrugi = ELAC.ZawodnikB.SelectedItem.ToString();
                int indexZawodnikPierwszy = ELAC.ZawodnikA.SelectedIndex + 1;
                int indexZawodnikDrugi = ELAC.ZawodnikB.SelectedIndex + 1;

                int intt = ELAC.Zawody.SelectedIndex + 1;
                string plec = ELAC.Pokaz.SelectedItem.ToString();
                string waga = ELAC.PokazWage.SelectedItem.ToString();
                string wiek = ELAC.PokazWiek.SelectedItem.ToString();
                string zawody = "Zawody" + intt + plec + "Kat" + waga + wiek;

                IL.ZamienMiejscami(zawody, zawodnikPierwszy, zawodnikDrugi, indexZawodnikPierwszy, indexZawodnikDrugi);
            }
            catch
            {
                MessageBox.Show("Nie udało się zamienić zawodników miejscami. Być może nie wszystkie wymagane pola są wypełnione.");
            }
        }

        private void EdytujZawodnika(object parameter)
        {
            try
            {
                int indeksZawodow = EZAC.Zawody.SelectedIndex + 1;
                int id = EZAC.Zawodnik.SelectedIndex + 1;
                string imie = EZAC.Imie.Text;
                string nazwisko = EZAC.Nazwisko.Text;
                string plec = EZAC.kobiety.IsChecked == true ? "K" : "M";
                string wiek = EZAC.KategoriaWiekowa.SelectedItem.ToString();
                string waga = EZAC.KategoriaWagowa.SelectedItem.ToString();
                string badania = EZAC.Nie.IsChecked == true ? "0" : "1";
                string licencja = EZAC.NumerLicencji.Text;
                string kraj = EZAC.Kraj.Text;
                string klub = EZAC.Klub.Text;
                string mail = EZAC.Mail.Text;
                string zawody = "Zawody" + indeksZawodow;

                IZ.EdytujZawodnika(id, imie, nazwisko, plec, wiek, waga, badania, licencja, kraj, klub, mail, zawody);
            }
            catch
            {
                MessageBox.Show("Edycja zawodnika nie powiodła się! Być może nie wszystkie wymagane pola są wypełnione.");
            }
        }
        
        private void EdytujZawody(object parameter)
        {
            try
            {
                int indeksZawodow = EEAC.Zawody.SelectedIndex + 1;
                string nazwa = EEAC.Nazwa.Text;
                string termin = EEAC.Termin.Text;
                string opis = EEAC.Opis.Text;
                string limit = EEAC.Limit.Text;
                string dyscyplina = (SEAC.boks.IsChecked == true) ? "0" : ((SEAC.kickboxing.IsChecked == true) ? "1" : "2");

                IZ.EdytujZawody(nazwa, termin, opis, limit, dyscyplina, indeksZawodow);
            }
            catch
            {
                MessageBox.Show("Edycja zawodów nie powiodła się! Być może nie wszystkie wymagane pola powyżej są wypełnione.");
            }
        }
    }
}