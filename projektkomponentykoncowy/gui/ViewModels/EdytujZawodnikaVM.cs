﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace GUI.ViewModels
{
    class EdytujZawodnikaVM : ViewModel
    {
        private Command powrot;
        private Command wyjscie;
        private Command wyczysc;
        private Command zapisz;

        public Command Powrot
        {
            get { return powrot; }
        }
        public Command Wyjscie
        {
            get { return wyjscie; }
        }
        public Command Wyczysc
        {
            get { return wyczysc; }
        }
        public Command Zapisz
        {
            get { return zapisz; }
        }
        public EdytujZawodnikaVM(Command powrot, Command wyjscie, Command wyczysc, Command zapisz)
        {
            this.powrot = powrot;
            this.wyjscie = wyjscie;
            this.wyczysc = wyczysc;
            this.zapisz = zapisz;
        }
    }
}
