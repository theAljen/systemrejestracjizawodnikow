﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace GUI.ViewModels
{
    class DrabinkaVM : ViewModel
    {
        private Command powrot;

        public Command Powrot
        {
            get { return powrot; }
        }

        public DrabinkaVM(Command powrot)
        {
            this.powrot = powrot;
        }
    }
}
