﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace GUI
{
    /// <summary>
    /// Interaction logic for Drabinka.xaml
    /// </summary>
    public partial class Drabinka : UserControl
    {

        /// <summary>
        /// Metoda zliczająca ilość rekordów w bazie zawodników dla konkretnych zawodów.
        /// </summary>
        /// <param name="idZawodow">ID zawodów, dla których ilość zawodników chcemy zliczyć.</param>
        /// <returns>Liczba zawodników zapisanych do konkretnych zawodów.</returns>
        public int LiczbaRekordowWBazieZawodnikow(int idZawodow)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");
            int indeks = 0;

            
            string kwerenda = string.Format("SELECT * FROM {0}","Zawody"+idZawodow);
            SqlCommand cmd = new SqlCommand(kwerenda, connection);

            connection.Open();
            SqlDataReader myreader = cmd.ExecuteReader();

            while (myreader.Read())
            {
                try
                {
                    indeks++;
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
            connection.Close();
            return indeks;

        }

        /// <summary>
        /// Metoda tworząca tablicę imion zawodników z tabeli "Zawody + INDEKS" bazy BazaZawodnikow.
        /// </summary>
        /// <param name="idZawodow">INDEKS zawodów/tabeli</param>
        /// <returns>Tablica imion zawodników z bazy</returns>
        public string[] StworzTabliceImion(int idZawodow)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");

            int id = 0;

            int liczbaRekorodw = LiczbaRekordowWBazieZawodnikow(idZawodow);
            string[] TablicaImion = new string[liczbaRekorodw];

            for (int i = 0; i < liczbaRekorodw; i++)
            {
                id++;

                string queryString = "SELECT * From {1} WHERE ZawodnikID ='{0}'";
                string query = String.Format(queryString, id, "Zawody"+idZawodow);
                SqlCommand cmd = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader czytacz = cmd.ExecuteReader();

                if (czytacz.Read())
                {
                    string imie = czytacz["Imie"].ToString();

                    TablicaImion[i] = imie;

                    connection.Close();
                }
                else
                {
                    connection.Close();
                }
            }
            return TablicaImion;

        }//koniec metody

        /// <summary>
        /// Metoda tworząca tablicę nazwisk zawodników z tabeli "Zawody + INDEKS" bazy BazaZawodnikow.
        /// </summary>
        /// <param name="idZawodow">INDEKS zawodów/tabeli</param>
        /// <returns>Tablica nazwisk zawodników z bazy</returns>
        public string[] StworzTabliceNazwisk(int idZawodow)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");

            int id = 0;

            int liczbaRekorodw = LiczbaRekordowWBazieZawodnikow(idZawodow);
            string[] TablicaNazwisk = new string[liczbaRekorodw];

            for (int i = 0; i < liczbaRekorodw; i++)
            {
                id++;

                string queryString = "SELECT * From {1} WHERE ZawodnikID ='{0}'";
                string query = String.Format(queryString, id, "Zawody"+idZawodow);
                SqlCommand cmd = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader czytacz = cmd.ExecuteReader();

                if (czytacz.Read())
                {
                    string nazwisko = czytacz["Nazwisko"].ToString();

                    TablicaNazwisk[i] = nazwisko;

                    connection.Close();
                }
                else
                {
                    connection.Close();
                }
            }
            return TablicaNazwisk;

        }//koniec metody


        /// <summary>
        /// Metoda tworząca tablicę wag zawodników z tabeli "Zawody + INDEKS" bazy BazaZawodnikow.
        /// </summary>
        /// <param name="idZawodow">INDEKS zawodów/tabeli</param>
        /// <returns>Tablica wag zawodników z bazy</returns>
        public string[] StworzTabliceWag(int idZawodow)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");

            int id = 0;

            int liczbaRekorodw = LiczbaRekordowWBazieZawodnikow(idZawodow);
            string[] TablicaWag = new string[liczbaRekorodw];

            for (int i = 0; i < liczbaRekorodw; i++)
            {
                id++;

                string queryString = "SELECT * From {1} WHERE ZawodnikID ='{0}'";
                string query = String.Format(queryString, id, "Zawody" + idZawodow);
                SqlCommand cmd = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader czytacz = cmd.ExecuteReader();

                if (czytacz.Read())
                {
                    string waga = czytacz["KategoriaWagowa"].ToString();

                    TablicaWag[i] = waga;

                    connection.Close();
                }
                else
                {
                    connection.Close();
                }
            }
            return TablicaWag;

        }//koniec metody


        /// <summary>
        /// Metoda tworząca tablicę kategorii wiekowych zawodników z tabeli "Zawody + INDEKS" bazy BazaZawodnikow.
        /// </summary>
        /// <param name="idZawodow">INDEKS zawodów/tabeli</param>
        /// <returns>Tablica kategorii wiekowych zawodników z bazy</returns>
        public string[] StworzTabliceWiekow(int idZawodow)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");

            int id = 0;

            int liczbaRekorodw = LiczbaRekordowWBazieZawodnikow(idZawodow);
            string[] TablicaWiekow = new string[liczbaRekorodw];

            for (int i = 0; i < liczbaRekorodw; i++)
            {
                id++;

                string queryString = "SELECT * From {1} WHERE ZawodnikID ='{0}'";
                string query = String.Format(queryString, id, "Zawody" + idZawodow);
                SqlCommand cmd = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader czytacz = cmd.ExecuteReader();

                if (czytacz.Read())
                {
                    string wiek = czytacz["KategoriaWiekowa"].ToString();

                    TablicaWiekow[i] = wiek;

                    connection.Close();
                }
                else
                {
                    connection.Close();
                }
            }
            return TablicaWiekow;

        }//koniec metody


        /// <summary>
        /// Metoda tworząca tablicę płci zawodników z tabeli "Zawody + INDEKS" bazy BazaZawodnikow.
        /// </summary>
        /// <param name="idZawodow">INDEKS zawodów/tabeli</param>
        /// <returns>Tablica płci zawodników z bazy</returns>
        public string[] StworzTablicePlci(int idZawodow)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");

            int id = 0;

            int liczbaRekorodw = LiczbaRekordowWBazieZawodnikow(idZawodow);
            string[] TablicaPlci = new string[liczbaRekorodw];

            for (int i = 0; i < liczbaRekorodw; i++)
            {
                id++;

                string queryString = "SELECT * From {1} WHERE ZawodnikID ='{0}'";
                string query = String.Format(queryString, id, "Zawody" + idZawodow);
                SqlCommand cmd = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader czytacz = cmd.ExecuteReader();

                if (czytacz.Read())
                {
                    string plec = czytacz["Plec"].ToString();

                    TablicaPlci[i] = plec;

                    connection.Close();
                }
                else
                {
                    connection.Close();
                }
            }
            return TablicaPlci;

        }//koniec metody

        /// <summary>
        /// Metoda wykonująca losowanie rozstawienia zawodników.
        /// </summary>
        /// <param name="idZawodow">ID zawodów, w których losowanie chcemy wykonać.</param>
        /// <param name="waga">Kategoria wagowa zawodników, dla której losowanie chcemy wykonać.</param>
        /// <param name="wiek">Kategoria wiekowa zawodników, dla której losowanie chcemy wykonać. Możliwości: SNR, JNR lub KDT.</param>
        /// <param name="plec">Płeć zawodników, dla której losowanie chcemy wykonać. Możliwości: M lub K.</param>
        /// <returns>Tablica imion i nazwisk zawodnikow, spełniajacych podane kryteria o losowej kolejności zawodników.</returns>
        public string[] Losuj(int idZawodow, int waga, string wiek, string plec)
        {
            int liczbaZawodnikow = LiczbaRekordowWBazieZawodnikow(idZawodow);

            
            List<string> ListaZawodnikow = new List<string>();
            List<string> ListaImionZawodnikow = new List<string>();
            
            string[] BazaNazwiskZawodnikow = StworzTabliceNazwisk(idZawodow);
            string[] BazaImionZawodnikow = StworzTabliceImion(idZawodow);
            string[] BazaWagZawodnikow = StworzTabliceWag(idZawodow);
            string[] BazaWiekowZawodnikow = StworzTabliceWiekow(idZawodow);
            string[] BazaPlciZawodnikow = StworzTablicePlci(idZawodow);

            for (int i = 0; i < liczbaZawodnikow; i++)
            {
                if ((BazaWagZawodnikow[i] == Convert.ToString(waga)) && (BazaWiekowZawodnikow[i] == wiek) && (BazaPlciZawodnikow[i] == plec))
                {
                    ListaZawodnikow.Add(BazaNazwiskZawodnikow[i]);
                    ListaImionZawodnikow.Add(BazaImionZawodnikow[i]);
                }              
            }

            string[] ZapamietanaTablicaLosowania = new string[ListaZawodnikow.Count()];

            Random losowa = new Random();

            int ilosc = ListaZawodnikow.Count;

            for (int i = 0; i < ilosc; i++)
            {
                int wylosowana = losowa.Next(0, ListaZawodnikow.Count);

                ZapamietanaTablicaLosowania[i] = ListaZawodnikow[wylosowana] + " " + ListaImionZawodnikow[wylosowana];

                ListaZawodnikow.RemoveAt(wylosowana);
                ListaImionZawodnikow.RemoveAt(wylosowana);
            }

            return ZapamietanaTablicaLosowania;

        }

        /// <summary>
        /// Metoda umieszczająca wykonane wcześniej losowanie w bazie danych. Dzięki niej wyniki losowań zapisują się po zamknięciu programu.
        /// </summary>
        /// <param name="losowanie">Tablica imion i nazwisk zawodników, ustawionych w odpowiedniej kolejności.</param>
        /// <param name="IDzawodow">ID zawodów, których losowanie chcemy umiescić w bazie.</param>
        /// <param name="waga">Kategoria wagowa zawodników, których chcemy umieścić w bazie.</param>
        /// <param name="wiek">Kategoria wiekowa zawodników, dla której losowanie chcemy wykonać. Możliwości: SNR, JNR lub KDT.</param>
        /// <param name="plec">Płeć zawodników, dla której losowanie chcemy wykonać. Możliwości: M lub K.</param>
        
        public void UmiescRozlosowanychZawodnikowWBazie(string[] losowanie, int IDzawodow, int waga, string wiek, string plec)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaLosowan.mdf" + @";Integrated Security=True");

            string query;
            SqlCommand cmd;

            int dlugoscTab = losowanie.Count();

            string nazwaTabeli = "Zawody" + Convert.ToString(IDzawodow) + plec + "Kat" + Convert.ToString(waga) + wiek;
            
            if (LiczbaRekordowWLosowaniu(nazwaTabeli) == 0)
            {
                
                    for (int i = 0; i < dlugoscTab; i++)
                    {
                        Polaczenie.Open();

                        query = String.Format("INSERT INTO {0} ([ImieNazwisko]) VALUES (@NazwaZawodnika)", nazwaTabeli);
                        cmd = new SqlCommand(query, Polaczenie);

                        cmd.Parameters.AddWithValue("@NazwaZawodnika", losowanie[i]);

                        cmd.ExecuteNonQuery();

                        Polaczenie.Close();
                    }
            }
        }

        /// <summary>
        /// Metoda zliczająca ilość rekordów w konkretnym losowaniu.
        /// </summary>
        /// <param name="nazwaLosowania">Nazwa tabeli z losowaniem, znajdujacej się w bazie BazaLosowań.mdf.</param>
        /// <returns>Liczba zawodników w konkretnej drabince.</returns>
         public int LiczbaRekordowWLosowaniu(string nazwaLosowania)
        {
            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaLosowan.mdf" + @";Integrated Security=True");
            int indeks = 0;

            string kwerenda = string.Format("SELECT * FROM {0}", nazwaLosowania);
            SqlCommand cmd = new SqlCommand(kwerenda, connection);

            connection.Open();
            SqlDataReader myreader = cmd.ExecuteReader();

            while (myreader.Read())
            {
                try
                {
                    indeks++;
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
            connection.Close();
            return indeks;

        }

        public Drabinka(string wiek, string plec,int zawodyID,int Waga)
        {

            InitializeComponent();

            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection sc = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaLosowan.mdf" + @";Integrated Security=True");
            String queryStringg = @"SELECT * From {0}";
            string wszystko = "Zawody" + zawodyID + plec + "Kat" + Waga + wiek;
            string[] tablica = new string[LiczbaRekordowWLosowaniu(wszystko)];

            String queryy = String.Format(queryStringg, wszystko);
            SqlCommand abcd = new SqlCommand(queryy, sc);
            SqlDataReader myreader;
            try
            {
                int i = 0;
                sc.Open();
                myreader = abcd.ExecuteReader();
                while (myreader.Read())
                {
                    tablica[i] = myreader["ImieNazwisko"].ToString();
                    i++;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            int liczbazawodnikow = tablica.Count();
            Label[] scoresLabelArr = new Label[5];
            int location = 230;
            int location2 = 280;
            int location4 = 180;
            int location44 = 220;
            int location8 = 160;
            int location16 = 18;
            int location1616 = 38;
            int location161616 = 78;
            int location16161616 = 158;
            int location32 = 10;
            int location3232 = 20;
            int location323232 = 40;
            int location32323232 = 80;

            if (liczbazawodnikow <= 2)
            {
                for (int i = 1; i <= 2; i++)
                {
                    TextBox label = new TextBox();
                    label.Height = 28;
                    label.Width = 200;
                    label.HorizontalAlignment = HorizontalAlignment.Left;
                    label.VerticalAlignment = VerticalAlignment.Top;
                    if (i <= LiczbaRekordowWLosowaniu(wszystko))
                    {
                        label.Text = tablica[i - 1];
                    }
                    else
                    {
                        label.Text = "";
                    }
                    label.Margin = new Thickness(30, location, 0, 0);
                    Grid.Children.Add(label);
                    location += 100;
                }
                for (int o = 0; o < 1; o++)
                {
                    TextBox label = new TextBox();
                    label.Height = 28;
                    label.Width = 200;
                    label.HorizontalAlignment = HorizontalAlignment.Left;
                    label.VerticalAlignment = VerticalAlignment.Top;
                    label.Margin = new Thickness(230, location2, 0, 0);
                    Grid.Children.Add(label);
                    label.Text = "WINNER";
                    location2 += 68;
                }
            }
            else
            {
                if (liczbazawodnikow <= 4)
                {
                    for (int i = 1; i <= 4; i++)
                    {
                        TextBox label = new TextBox();
                        label.Height = 28;
                        label.Width = 200;
                        label.HorizontalAlignment = HorizontalAlignment.Left;
                        label.VerticalAlignment = VerticalAlignment.Top;
                        if (i <= LiczbaRekordowWLosowaniu(wszystko))
                        {
                            label.Text = tablica[i - 1];
                        }
                        else
                        {
                            label.Text = "";
                        }

                        label.Margin = new Thickness(30, location4, 0, 0);
                        Grid.Children.Add(label);
                        location4 += 80;
                    }
                    for (int o = 0; o < 2; o++)
                    {
                        TextBox label = new TextBox();
                        label.Height = 28;
                        label.Width = 200;
                        label.HorizontalAlignment = HorizontalAlignment.Left;
                        label.VerticalAlignment = VerticalAlignment.Top;
                        label.Margin = new Thickness(230, location44, 0, 0);
                        Grid.Children.Add(label);
                        location44 += 160;
                    }
                    for (int o = 0; o < 1; o++)
                    {
                        TextBox label = new TextBox();
                        label.Height = 28;
                        label.Width = 200;
                        label.HorizontalAlignment = HorizontalAlignment.Left;
                        label.VerticalAlignment = VerticalAlignment.Top;
                        label.Margin = new Thickness(430, 300, 0, 0);
                        label.Text = "WINNER";
                        Grid.Children.Add(label);
                    }
                }
                else
                {
                    if (liczbazawodnikow <= 8)
                    {
                        for (int i = 1; i <= 8; i++)
                        {
                            TextBox label = new TextBox();
                            label.Height = 28;
                            label.Width = 200;
                            label.HorizontalAlignment = HorizontalAlignment.Left;
                            label.VerticalAlignment = VerticalAlignment.Top;
                            if (i <= LiczbaRekordowWLosowaniu(wszystko))
                            {
                                label.Text = tablica[i - 1];
                            }
                            else
                            {
                                label.Text = "";
                            }
                            label.Margin = new Thickness(30, location8, 0, 0);
                            Grid.Children.Add(label);
                            location8 += 40;
                        }
                        for (int i = 1; i <= 4; i++)
                        {
                            TextBox label = new TextBox();
                            label.Height = 28;
                            label.Width = 200;
                            label.HorizontalAlignment = HorizontalAlignment.Left;
                            label.VerticalAlignment = VerticalAlignment.Top;
                            label.Margin = new Thickness(230, location4, 0, 0);
                            Grid.Children.Add(label);
                            location4 += 80;
                        }
                        for (int o = 0; o < 2; o++)
                        {
                            TextBox label = new TextBox();
                            label.Height = 28;
                            label.Width = 200;
                            label.HorizontalAlignment = HorizontalAlignment.Left;
                            label.VerticalAlignment = VerticalAlignment.Top;
                            label.Margin = new Thickness(430, location44, 0, 0);
                            Grid.Children.Add(label);
                            location44 += 160;
                        }
                        for (int o = 0; o < 1; o++)
                        {
                            TextBox label = new TextBox();
                            label.Height = 28;
                            label.Width = 200;
                            label.HorizontalAlignment = HorizontalAlignment.Left;
                            label.VerticalAlignment = VerticalAlignment.Top;
                            label.Margin = new Thickness(630, 300, 0, 0);
                            label.Text = "WINNER";
                            Grid.Children.Add(label);
                        }
                    }
                    else
                    {
                        if (liczbazawodnikow <= 16)
                        {
                            for (int i = 1; i <= 16; i++)
                            {
                                TextBox label = new TextBox();
                                label.Height = 28;
                                label.Width = 200;
                                label.HorizontalAlignment = HorizontalAlignment.Left;
                                label.VerticalAlignment = VerticalAlignment.Top;
                                if (i <= LiczbaRekordowWLosowaniu(wszystko))
                                {
                                    label.Text = tablica[i - 1];
                                }
                                else
                                {
                                    label.Text = "";
                                }
                                label.Margin = new Thickness(10, location16, 0, 0);
                                Grid.Children.Add(label);
                                location16 += 40;
                            }
                            for (int i = 1; i <= 8; i++)
                            {
                                TextBox label = new TextBox();
                                label.Height = 28;
                                label.Width = 200;
                                label.HorizontalAlignment = HorizontalAlignment.Left;
                                label.VerticalAlignment = VerticalAlignment.Top;
                                label.Margin = new Thickness(210, location1616, 0, 0);
                                Grid.Children.Add(label);
                                location1616 += 80;
                            }
                            for (int i = 1; i <= 4; i++)
                            {
                                TextBox label = new TextBox();
                                label.Height = 28;
                                label.Width = 200;
                                label.HorizontalAlignment = HorizontalAlignment.Left;
                                label.VerticalAlignment = VerticalAlignment.Top;
                                label.Margin = new Thickness(410, location161616, 0, 0);
                                Grid.Children.Add(label);
                                location161616 += 160;
                            }
                            for (int o = 0; o < 2; o++)
                            {
                                TextBox label = new TextBox();
                                label.Height = 28;
                                label.Width = 200;
                                label.HorizontalAlignment = HorizontalAlignment.Left;
                                label.VerticalAlignment = VerticalAlignment.Top;
                                label.Margin = new Thickness(610, location16161616, 0, 0);
                                Grid.Children.Add(label);
                                location16161616 += 320;
                            }
                            for (int o = 0; o < 1; o++)
                            {
                                TextBox label = new TextBox();
                                label.Height = 28;
                                label.Width = 200;
                                label.HorizontalAlignment = HorizontalAlignment.Left;
                                label.VerticalAlignment = VerticalAlignment.Top;
                                label.Margin = new Thickness(770, 318, 0, 0);
                                label.Text = "WINNER";
                                Grid.Children.Add(label);
                            }
                        }
                        else
                        {
                            if (liczbazawodnikow <= 32)
                            {
                                for (int i = 1; i <= 32; i++)
                                {
                                    TextBox label = new TextBox();
                                    label.Height = 20;
                                    label.Width = 170;
                                    label.HorizontalAlignment = HorizontalAlignment.Left;
                                    label.VerticalAlignment = VerticalAlignment.Top;
                                    if (i <= LiczbaRekordowWLosowaniu(wszystko))
                                    {
                                        label.Text = tablica[i - 1];
                                    }
                                    else
                                    {
                                        label.Text = "";
                                    }
                                    label.Margin = new Thickness(10, location32, 0, 0);
                                    Grid.Children.Add(label);
                                    location32 += 20;
                                }
                                for (int i = 1; i <= 16; i++)
                                {
                                    TextBox label = new TextBox();
                                    label.Height = 20;
                                    label.Width = 170;
                                    label.HorizontalAlignment = HorizontalAlignment.Left;
                                    label.VerticalAlignment = VerticalAlignment.Top;
                                   
                                    label.Margin = new Thickness(180, location3232, 0, 0);
                                    Grid.Children.Add(label);
                                    location3232 += 40;
                                }
                                for (int i = 1; i <= 8; i++)
                                {
                                    TextBox label = new TextBox();
                                    label.Height = 20;
                                    label.Width = 170;
                                    label.HorizontalAlignment = HorizontalAlignment.Left;
                                    label.VerticalAlignment = VerticalAlignment.Top;
                                    label.Margin = new Thickness(350, location323232, 0, 0);
                                    Grid.Children.Add(label);
                                    location323232 += 80;
                                }
                                for (int i = 1; i <= 4; i++)
                                {
                                    TextBox label = new TextBox();
                                    label.Height = 20;
                                    label.Width = 170;
                                    label.HorizontalAlignment = HorizontalAlignment.Left;
                                    label.VerticalAlignment = VerticalAlignment.Top;
                                    label.Margin = new Thickness(500, location32323232, 0, 0);
                                    Grid.Children.Add(label);
                                    location32323232 += 160;
                                }
                                for (int o = 0; o < 2; o++)
                                {
                                    TextBox label = new TextBox();
                                    label.Height = 20;
                                    label.Width = 170;
                                    label.HorizontalAlignment = HorizontalAlignment.Left;
                                    label.VerticalAlignment = VerticalAlignment.Top;
                                    label.Margin = new Thickness(650, location16161616, 0, 0);
                                    Grid.Children.Add(label);
                                    location16161616 += 320;
                                }
                                for (int o = 0; o < 1; o++)
                                {
                                    TextBox label = new TextBox();
                                    label.Height = 20;
                                    label.Width = 170;
                                    label.HorizontalAlignment = HorizontalAlignment.Left;
                                    label.VerticalAlignment = VerticalAlignment.Top;
                                    label.Margin = new Thickness(800, 318, 0, 0);
                                    label.Text = "WINNER";
                                    Grid.Children.Add(label);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Wprowadzono zbyt wielu zawodników!");
                            }
                        }
                    }
                }
            }

        }
    }
}