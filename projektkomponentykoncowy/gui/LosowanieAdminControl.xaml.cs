﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Data.SqlClient;

namespace GUI
{
    /// <summary>
    /// Interaction logic for LosowanieAdminControl.xaml
    /// </summary>
    public partial class LosowanieAdminControl : UserControl
    {
        public LosowanieAdminControl()
        {
            InitializeComponent();

            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodow.mdf" + @";Integrated Security=True");

            String queryString = "SELECT * From Zawody ";
            String query = String.Format(queryString);
            SqlCommand abc = new SqlCommand(query, Polaczenie);
            Polaczenie.Open();
            SqlDataReader myreader = abc.ExecuteReader();
            while (myreader.Read())
            {
                string imie = myreader["Nazwa"].ToString();
                Zawody.Items.Add(imie);
            }
        }

        private void Akceptacja_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Pokaz.Items.Clear();
                PokazWiek.Items.Clear();
                PokazWage.Items.Clear();

                string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                directory = directory.Substring(0, directory.Length - 14);

                SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodow.mdf" + @";Integrated Security=True");
                String queryStringg = "SELECT * From Zawody WHERE ZawodyID ='{0}'";
                String queryy = String.Format(queryStringg, Convert.ToInt32(Zawody.SelectedIndex + 1));
                SqlCommand abcd = new SqlCommand(queryy, Polaczenie);
                SqlDataReader myreaderr;

                //try
                //{
                    Polaczenie.Open();
                    myreaderr = abcd.ExecuteReader();
                    while (myreaderr.Read())
                    {

                        string Waga54 = myreaderr["Czy54"].ToString();
                        if (Waga54.Equals("True"))
                        {
                            PokazWage.Items.Add("54");
                        }

                        string Waga57 = myreaderr["Czy57"].ToString();
                        if (Waga57.Equals("True"))
                        {
                            PokazWage.Items.Add("57");
                        }

                        string Waga63 = myreaderr["Czy63"].ToString();
                        if (Waga63.Equals("True"))
                        {
                            PokazWage.Items.Add("63");
                        }

                        string Waga69 = myreaderr["Czy69"].ToString();
                        if (Waga69.Equals("True"))
                        {
                            PokazWage.Items.Add("69");
                        }

                        string Waga74 = myreaderr["Czy74"].ToString();
                        if (Waga74.Equals("True"))
                        {
                            PokazWage.Items.Add("74");
                        }

                        string Waga79 = myreaderr["Czy79"].ToString();
                        if (Waga79.Equals("True"))
                        {
                            PokazWage.Items.Add("79");
                        }

                        string Waga84 = myreaderr["Czy84"].ToString();
                        if (Waga84.Equals("True"))
                        {
                            PokazWage.Items.Add("84");
                        }

                        string Waga89 = myreaderr["Czy89"].ToString();
                        if (Waga89.Equals("True"))
                        {
                            PokazWage.Items.Add("89");
                        }

                        string Waga94 = myreaderr["Czy94"].ToString();
                        if (Waga94.Equals("True"))
                        {
                            PokazWage.Items.Add("94");
                        }


                        string CzySenior = myreaderr["CzySenior"].ToString();
                        if (CzySenior.Equals("True"))
                        {
                            PokazWiek.Items.Add("SNR");
                        }

                        string CzyKadet = myreaderr["CzyKadet"].ToString();
                        if (CzyKadet.Equals("True"))
                        {
                            PokazWiek.Items.Add("KDT");
                        }

                        string CzyJunior = myreaderr["CzyJunior"].ToString();
                        if (CzyJunior.Equals("True"))
                        {
                            PokazWiek.Items.Add("JNR");
                        }
                        if (Akceptacja.IsMouseOver)
                        {
                            string plec = myreaderr["Plec"].ToString();
                            if (plec == "1")
                            {
                                Pokaz.Items.Add("M");
                            }
                            else
                            {
                                Pokaz.Items.Add("K");
                            }
                        }

                    }

                //}
                //catch (Exception ex)
                //{

                //    MessageBox.Show(ex.Message);
                //}
            }
            catch
            {
                MessageBox.Show("Proszę najpierw wybrać zawody z listy.");
            }

        }//koniec metody


    }
}