﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.IO;

namespace GUI
{
    /// <summary>
    /// Interaction logic for EdytujZawodnikaAdmin.xaml
    /// </summary>
    public partial class EdytujZawodnikaAdmin : UserControl
    {
        public EdytujZawodnikaAdmin()
        {
            InitializeComponent();

            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodow.mdf" + @";Integrated Security=True");
            String queryString = "SELECT * From Zawody ";
            String query = String.Format(queryString);
            SqlCommand abc = new SqlCommand(query, Polaczenie);
            Polaczenie.Open();
            SqlDataReader myreader = abc.ExecuteReader();
            while (myreader.Read())
            {
                string imie = myreader["Nazwa"].ToString();
                Zawody.Items.Add(imie);
            }
            KategoriaWagowa.Items.Add("54");
            KategoriaWagowa.Items.Add("57");
            KategoriaWagowa.Items.Add("63");
            KategoriaWagowa.Items.Add("69");
            KategoriaWagowa.Items.Add("74");
            KategoriaWagowa.Items.Add("79");
            KategoriaWagowa.Items.Add("84");
            KategoriaWagowa.Items.Add("89");
            KategoriaWagowa.Items.Add("94");

            KategoriaWiekowa.Items.Add("SNR");
            KategoriaWiekowa.Items.Add("KDT");
            KategoriaWiekowa.Items.Add("JNR");
        }

        private void aktualizuj_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Zawodnik.Items.Clear();

                string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                directory = directory.Substring(0, directory.Length - 14);

                SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodnikow.mdf" + @";Integrated Security=True");
                String queryStringg = "SELECT * From {0}";
                int intt = Zawody.SelectedIndex + 1;

                String queryy = String.Format(queryStringg, "Zawody" + intt);
                SqlCommand abc = new SqlCommand(queryy, Polaczenie);
                Polaczenie.Open();
                SqlDataReader myreader = abc.ExecuteReader();
                while (myreader.Read())
                {
                    string imie = myreader["Imie"].ToString();
                    string nazwisko = myreader["Nazwisko"].ToString();
                    Zawodnik.Items.Add(imie + " " + nazwisko);
                }
            }
            catch
            {
                MessageBox.Show("Najpierw należy wybrać zawody z rozwijanej listy");
            }
        }


    }
}
