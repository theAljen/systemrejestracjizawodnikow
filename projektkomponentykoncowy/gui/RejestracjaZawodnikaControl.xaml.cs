﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.IO;


namespace GUI
{
    /// <summary>
    /// Interaction logic for RejestracjaZawodnikaControl.xaml
    /// </summary>
    public partial class RejestracjaZawodnikaControl : UserControl
    {
        public RejestracjaZawodnikaControl()
        {
            InitializeComponent();

            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodow.mdf" + @";Integrated Security=True");
            String queryString = "SELECT * From Zawody ";
            String query = String.Format(queryString);
            SqlCommand abc = new SqlCommand(query, Polaczenie);
            Polaczenie.Open();
            SqlDataReader myreader = abc.ExecuteReader();
            while (myreader.Read())
            {
                string imie = myreader["Nazwa"].ToString();
                Zawody.Items.Add(imie);
            }
        }
    }

}