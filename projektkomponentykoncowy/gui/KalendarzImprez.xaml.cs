﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SystemKalendarza.Contract;

namespace GUI
{
    /// <summary>
    /// Interaction logic for KalendarzImprez.xaml
    /// </summary>
    public partial class KalendarzImprez : UserControl
    {
        private List<Zawody> zawodyList;

        private KalendarzImprez()
        {
            InitializeComponent();

            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            directory = directory.Substring(0, directory.Length - 14);

            SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodow.mdf" + @";Integrated Security=True");

            String queryString = "SELECT * From Zawody ";
            String query = String.Format(queryString);
            SqlCommand abc = new SqlCommand(query, Polaczenie);
            Polaczenie.Open();
            SqlDataReader myreader = abc.ExecuteReader();
            while (myreader.Read())
            {
                string imie = myreader["Nazwa"].ToString();
                Zawody.Items.Add(imie);
            }
        }

        public KalendarzImprez(List<Zawody> zawodyList)
        {
            InitializeComponent();
            this.zawodyList = zawodyList;
            foreach(Zawody z in zawodyList)
            {
                Zawody.Items.Add(z.nazwa);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Pokaz.Items.Clear();
                PokazWiek.Items.Clear();
                PokazWage.Items.Clear();
                Wiek.Text = "Kategorie wiekowe w jakich odbędą się rozgrywki:";
                Nazwa.Text = "Nazwa:";
                Opis.Text = "Opis:";
                Termin.Text = "Termin:";

                string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                directory = directory.Substring(0, directory.Length - 14);

                SqlConnection Polaczenie = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + directory + @"\BazyDanych\BazaZawodow.mdf" + @";Integrated Security=True");
                String queryStringg = "SELECT * From Zawody WHERE ZawodyID ='{0}'";
                String queryy = String.Format(queryStringg, Convert.ToInt32(Zawody.SelectedIndex + 1));
                SqlCommand abcd = new SqlCommand(queryy, Polaczenie);
                SqlDataReader myreaderr;

                //try
                //{
                    Polaczenie.Open();
                    myreaderr = abcd.ExecuteReader();
                    while (myreaderr.Read())
                    {
                        string sNazwa = myreaderr["Nazwa"].ToString();
                        if (sNazwa != "")
                        {
                            Nazwa.Text = "Nazwa: " + sNazwa;
                        }
                        else
                        {
                            Nazwa.Text = "Nazwa:";
                        }

                        string daaata = myreaderr["Termin"].ToString();
                        Termin.Text = "Termin: " + daaata;

                        string opis = myreaderr["Opis"].ToString();
                        if (opis != "")
                        {
                            Opis.Text = "Opis: " + opis;
                        }
                        else
                        {
                            Opis.Text = "Opis:";
                        }


                        waga.Text = "Kategorie wagowe w jakich odbędą się rozgrywki:";

                        string Waga54 = myreaderr["Czy54"].ToString();
                        if (Waga54.Equals("True"))
                        {
                            waga.Text = "Kategorie wagowe w jakich odbędą się rozgrywki:54kg, ";
                            PokazWage.Items.Add("54");
                        }

                        string Waga57 = myreaderr["Czy57"].ToString();
                        if (Waga57.Equals("True"))
                        {
                            waga.Text = waga.Text + "57kg, ";
                            PokazWage.Items.Add("57");
                        }

                        string Waga63 = myreaderr["Czy63"].ToString();
                        if (Waga63.Equals("True"))
                        {
                            waga.Text = waga.Text + "63kg, ";
                            PokazWage.Items.Add("63");
                        }

                        string Waga69 = myreaderr["Czy69"].ToString();
                        if (Waga69.Equals("True"))
                        {
                            waga.Text = waga.Text + "69kg, ";
                            PokazWage.Items.Add("69");
                        }

                        string Waga74 = myreaderr["Czy74"].ToString();
                        if (Waga74.Equals("True"))
                        {
                            waga.Text = waga.Text + "74kg, ";
                            PokazWage.Items.Add("74");
                        }

                        string Waga79 = myreaderr["Czy79"].ToString();
                        if (Waga79.Equals("True"))
                        {
                            waga.Text = waga.Text + "79kg, ";
                            PokazWage.Items.Add("79");
                        }

                        string Waga84 = myreaderr["Czy84"].ToString();
                        if (Waga84.Equals("True"))
                        {
                            waga.Text = waga.Text + "84kg, ";
                            PokazWage.Items.Add("84");
                        }

                        string Waga89 = myreaderr["Czy89"].ToString();
                        if (Waga89.Equals("True"))
                        {
                            waga.Text = waga.Text + "89kg, ";
                            PokazWage.Items.Add("89");
                        }

                        string Waga94 = myreaderr["Czy94"].ToString();
                        if (Waga94.Equals("True"))
                        {
                            waga.Text = waga.Text + "94kg";
                            PokazWage.Items.Add("94");
                        }


                        Wiek.Text = "Kategorie wiekowe w jakich odbędą się rozgrywki:";

                        string CzySenior = myreaderr["CzySenior"].ToString();
                        if (CzySenior.Equals("True"))
                        {
                            Wiek.Text = "Kategorie wiekowe w jakich odbędą się rozgrywki:Senior, ";
                            PokazWiek.Items.Add("SNR");
                        }

                        string CzyKadet = myreaderr["CzyKadet"].ToString();
                        if (CzyKadet.Equals("True"))
                        {
                            Wiek.Text = Wiek.Text + "Kadet, ";
                            PokazWiek.Items.Add("KDT");
                        }

                        string CzyJunior = myreaderr["CzyJunior"].ToString();
                        if (CzyJunior.Equals("True"))
                        {
                            Wiek.Text = Wiek.Text + "Junior";
                            PokazWiek.Items.Add("JNR");
                        }
                        if (aktualizuj.IsMouseOver)
                        {
                            string plec = myreaderr["Plec"].ToString();
                            if (plec == "1")
                            {
                                Pokaz.Items.Add("M");
                            }
                            else
                            {
                                Pokaz.Items.Add("K");
                            }
                        }

                    }

                //}
                //catch (Exception ex)
                //{

                //    MessageBox.Show(ex.Message);
                //}
            }
            catch
            {
                MessageBox.Show("Błąd! Być może nie wybrano zawodów z listy.");
            }

        }

        private void PokazDrabinke_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}