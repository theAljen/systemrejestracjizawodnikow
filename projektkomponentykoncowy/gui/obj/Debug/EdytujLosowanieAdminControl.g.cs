﻿#pragma checksum "..\..\EdytujLosowanieAdminControl.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F0C7C2707CAFEEB0740CE8494E34B11D"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GUI {
    
    
    /// <summary>
    /// EdytujLosowanieAdminControl
    /// </summary>
    public partial class EdytujLosowanieAdminControl : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\EdytujLosowanieAdminControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Zawody;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\EdytujLosowanieAdminControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button aktualizuj;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\EdytujLosowanieAdminControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox PokazWage;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\EdytujLosowanieAdminControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox PokazWiek;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\EdytujLosowanieAdminControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Pokaz;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\EdytujLosowanieAdminControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button aktualizuj2;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\EdytujLosowanieAdminControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ZawodnikA;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\EdytujLosowanieAdminControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ZawodnikB;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\EdytujLosowanieAdminControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Powrot;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\EdytujLosowanieAdminControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Losuj;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GUI;component/edytujlosowanieadmincontrol.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\EdytujLosowanieAdminControl.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Zawody = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 2:
            this.aktualizuj = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\EdytujLosowanieAdminControl.xaml"
            this.aktualizuj.Click += new System.Windows.RoutedEventHandler(this.aktualizuj_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.PokazWage = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 4:
            this.PokazWiek = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 5:
            this.Pokaz = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 6:
            this.aktualizuj2 = ((System.Windows.Controls.Button)(target));
            
            #line 19 "..\..\EdytujLosowanieAdminControl.xaml"
            this.aktualizuj2.Click += new System.Windows.RoutedEventHandler(this.aktualizuj_Click_1);
            
            #line default
            #line hidden
            return;
            case 7:
            this.ZawodnikA = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.ZawodnikB = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 9:
            this.Powrot = ((System.Windows.Controls.Button)(target));
            return;
            case 10:
            this.Losuj = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

