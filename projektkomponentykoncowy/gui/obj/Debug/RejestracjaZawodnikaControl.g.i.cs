﻿#pragma checksum "..\..\RejestracjaZawodnikaControl.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "5F07A1C885065A5A868A787764E1250A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GUI {
    
    
    /// <summary>
    /// RejestracjaZawodnikaControl
    /// </summary>
    public partial class RejestracjaZawodnikaControl : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Zawody;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Akceptacja;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Imie;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Nazwisko;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton kobiety;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton mezczyzni;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox KategoriaWagowa;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox KategoriaWiekowa;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox NumerLicencji;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton Tak;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton Nie;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Kraj;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Klub;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Mail;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button WyczyscFormularz;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ZarejestrujZawodnika;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button wyjscie;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\RejestracjaZawodnikaControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Powrot;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GUI;component/rejestracjazawodnikacontrol.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\RejestracjaZawodnikaControl.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Zawody = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 2:
            this.Akceptacja = ((System.Windows.Controls.Button)(target));
            return;
            case 3:
            this.Imie = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.Nazwisko = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.kobiety = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 6:
            this.mezczyzni = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 7:
            this.KategoriaWagowa = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.KategoriaWiekowa = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 9:
            this.NumerLicencji = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.Tak = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 11:
            this.Nie = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 12:
            this.Kraj = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.Klub = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.Mail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.WyczyscFormularz = ((System.Windows.Controls.Button)(target));
            return;
            case 16:
            this.ZarejestrujZawodnika = ((System.Windows.Controls.Button)(target));
            return;
            case 17:
            this.wyjscie = ((System.Windows.Controls.Button)(target));
            return;
            case 18:
            this.Powrot = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

