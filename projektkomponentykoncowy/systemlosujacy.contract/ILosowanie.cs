﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemLosujacy.Contract
{
    public interface ILosowanie
    {
        string[] Losuj(int idZawodow, int waga, string wiek, string plec);
        void UmiescRozlosowanychZawodnikowWBazie(string[] losowanie, int IDzawodow, int waga, string wiek, string plec);
        void ZamienMiejscami(string zawody, string zawodnikPierwszy, string zawodnikDrugi, int indexZawodnikPierwszy, int indexZawodnikDrugi);
    }
}
