﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uzytkownicy.Contract
{
    public interface ITrener
    {
        void DodajZawodnika(int dodawanie, string imie, string nazwisko, string plec, string wiek, string waga, string badania, string licencja, string kraj, string klub, string mail);
    }
}
